<html>
	<head>
		<title>г. Минск - Сухаревская, 5</title>
	</head>
	<body>
		<img id="baseImage8" width="100%"/>
		<img id="baseImage7" width="100%"/>
		<img id="baseImage6" width="100%"/>
		<img id="baseImage5" width="100%"/>
		<img id="baseImage4" width="100%"/>
		<img id="baseImage3" width="100%"/>
		<img id="baseImage2" width="100%"/>
		<img id="baseImage1" width="100%"/>
	</body>
</html>
<script type="text/javascript">
	function refreshCam(num) {
		const url = "./camera.ajax.php?i=" + num;
		let xhr = new XMLHttpRequest();
		xhr.open('POST', url, true)
		xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8')
		xhr.send();
		xhr.onload = function (answer) {
			if(xhr.status === 200) {
				document.getElementById("baseImage" + num).src = "data:image;base64," + answer.currentTarget.response;
			}
		}
	}
	async function initCams() {
		refreshCam(8);
		setInterval(function() {refreshCam(8);}, 8000);
		await new Promise(r => setTimeout(r, 1000));

		refreshCam(7);
		setInterval(function() {refreshCam(7);}, 8000);
		await new Promise(r => setTimeout(r, 1000));

		refreshCam(6);
		setInterval(function() {refreshCam(6);}, 8000);
		await new Promise(r => setTimeout(r, 1000));

		refreshCam(5);
		setInterval(function() {refreshCam(5);}, 8000);
		await new Promise(r => setTimeout(r, 1000));

		refreshCam(4);
		setInterval(function() {refreshCam(4);}, 8000);
		await new Promise(r => setTimeout(r, 1000));

		refreshCam(3);
		setInterval(function() {refreshCam(3);}, 8000);
		await new Promise(r => setTimeout(r, 1000));

		refreshCam(2);
		setInterval(function() {refreshCam(2);}, 8000);
		await new Promise(r => setTimeout(r, 1000));

		refreshCam(1);
		setInterval(function() {refreshCam(1);}, 8000);
		await new Promise(r => setTimeout(r, 1000));
	}
	initCams();

</script>
