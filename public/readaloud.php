<?php
	$content = $_GET["txt"];
	$spans = preg_split("/([.!?:;])/", $content, -1, PREG_SPLIT_DELIM_CAPTURE);
	$final = [];
	for ($i = 0; $i < count($spans); $i++) {
		if (strlen($spans[$i]) < 10 && strlen($final[count($final)-1]) < 500 && $i > 0) {
			$final[count($final)-1] .=  $spans[$i];
		} else {
			array_push($final, $spans[$i]);
		}
	}
	echo '
	<style>
		span {
			font-size:1.65vmax;
			white-space: pre-wrap;
			line-height: 1.4;
			scroll-margin-top: 100px;
		}

:root {
    --button-height: 2rem;
    --button-color: #edd;
}

html {
    font-size: 30px;
}

body {
    padding: 3vw;
    margin: 0;
}

		.separator {
			height: 50px;
		}
		.fake-player {
			display: block;
			margin: auto;
			justify-content: center;
			align-items: center;
			width: 4rem;
			padding: 2rem;
			border: 0.2rem solid var(--button-color);
			border-radius: 50%;
			filter: drop-shadow(0 0 3.1rem rgba(255,255,255, 0.8));
		}

		button {
			margin: 0;
			padding: 0;
		}

		.play {
			height: 0;
			width: 0;
			margin-left: calc(2 * 0.14 * var(--button-height));
			/*margin-left: 17px;*/
			background: none;
			border: none;
			border-top: var(--button-height) solid transparent;
			border-bottom: var(--button-height) solid transparent;
			border-left: calc(var(--button-height) * 2 * 0.86) solid var(--button-color);
			
		}

		.pause {
			position: relative;
			background: none;
			border: none;
			height: calc(var(--button-height) * 2);
			width: calc(var(--button-height) * 2 * 0.86);
			&:before, &:after {
				content: "";
				position: absolute;
				top: 0;
				height: 100%;
				width: 33%;
				background: var(--button-color);
			}
			&:before {
				left: 0;
			}
			&:after {
				right: 0;
			}
		}

		.hidden {
			display:none;
		}
	</style>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<body>
	<div class="content">';

	foreach ($final as $key => $part) {
		echo "<span id=\"s$key\">" . $part . "</span>";
	}

	echo '
	</div>
	<div class="separator"></div>
	<div class="fake-player">
		<button role="play" class="play"></button>
		<button role="pause" class="pause hidden"></button>
	</div>
	</body>
';
?>
<script>

	const player = document.querySelector('.fake-player');

	function clickHandler () { 
		const buttons = Array.from(this.children);
		buttons.forEach(button => button.classList.toggle('hidden'))
		readToggle();
	};

	player.addEventListener('click', clickHandler);

	let currentPoint = "0";
	let audioElement = new Audio();
	audioElement.preload = "auto";
	audioElement.autoplay = true;
	let pause = true;
	let totalPoints = document.getElementsByTagName('span').length;
	const selection = window.getSelection();
	const range = document.createRange();

	window.onclick = async function(e) {
		if (e.target.tagName == 'SPAN') {
			currentPoint = e.target.id.replace('s', '');
			selectCurrentElement(currentPoint);
			e.stopPropagation();
		}
	} 

	document.body.onkeydown = function(e) {
		if (e.keyCode == 32 && e.target == document.body) {
			player.dispatchEvent(new Event('click'));
			e.preventDefault();
		}
		if (e.keyCode == 38 && e.target == document.body) {
			goUp();
		}
		if (e.keyCode == 40 && e.target == document.body) {
			goDown();
		}
	}

	document.onreadystatechange = async function () {
		if (document.readyState == "complete") {
			selectCurrentElement(currentPoint);
		}
	}

	async function goUp() {
		let wasStoppedReading = false;
		if (!pause) {
			stopreading();
			wasStoppedReading = true;
		}
		currentPoint--;
		selectCurrentElement(currentPoint);
		if (wasStoppedReading) {
			await readthebook();
		}
	}

	async function goDown() {
		let wasStoppedReading = false;
		if (!pause) {
			stopreading();
			wasStoppedReading = true;
		}
		currentPoint++;
		selectCurrentElement(currentPoint);
		if (wasStoppedReading) {
			await readthebook();
		}
	}

	function readToggle() {
		if (pause) {
			readthebook();
		} else {
			stopreading();
		}
	}

	async function readthebook() {
		pause = false;
		for (i = currentPoint; i < totalPoints; i++) {
			element = document.querySelector('#s' + i)
			selectCurrentElement(currentPoint);
			await play("./get_audio.ajax.php" + 
				"?currentPoint=" + currentPoint + 
				"&sentence=" + encodeURI(element.textContent) + 
				"&rate=0" + 
				"&voice=anna" + 
				"&totalPoints=" + totalPoints);
			currentPoint++;
		}
		pause = true;
		currentPoint = 0;
		const buttons = Array.from(player.children);
		buttons.forEach(button => button.classList.toggle('hidden'))
	}

	function stopreading() {
		pause = true;
		audioElement.pause();
		selectCurrentElement(currentPoint);
	}

	function selectCurrentElement(number) {
		let element = document.querySelector('#s' + number);
		element.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
		highlightElement(number);
	}

	function highlightElement(number) {
		let element = document.querySelector('#s' + number);
		if (window.getSelection) {
			range.selectNodeContents(element);
			selection.removeAllRanges();
			selection.addRange(range);
		}
	}

	async function play(url) {
		return await new Promise(function(resolve, reject) {
			audioElement.onerror = resolve;
			audioElement.onended = resolve;
			audioElement.onstalled = e => {currentPoint--; resolve();}
			audioElement.src = url;
		});
	}
 
	const toggle = (targetId) => {
	  const dots = document.getElementById(`dots-${targetId}`);
	  const toggle = document.getElementById(`myBtn-${targetId}`);
	  const less = document.getElementById(`less-${targetId}`);
	  const more = document.getElementById(`more-${targetId}`);
	  const y = dots.style.display === "none";
	  less.setAttribute("style", "white-space: " + (y ? "normal" : "pre-wrap"));
	  more.setAttribute("style", "white-space: " + (y ? "normal" : "pre-wrap"));
	  dots.style.display = y ? "inline" : "none";
	  more.style.display = y ? "none" : "inline";
	};

	const showParagraph = (targetId, wordText, paragraphText, displayLimit) => {
	
		const pTagElem = document.createElement("p");
		pTagElem.setAttribute("style", "word-break: keep-all;");

		const uniqueId = targetId || "body";

		const dotsSpanElem = document.createElement("span");
		dotsSpanElem.id = `dots-${uniqueId}`;
		if (paragraphText.length > displayLimit) {
			dotsSpanElem.textContent = "...";
		}
		dotsSpanElem.setAttribute("style", "display: inline");

		const lessTextSpanElem = document.createElement("span");
		lessTextSpanElem.id = `less-${uniqueId}`;
		lessTextSpanElem.setAttribute("style", "white-space: normal;");

		const moreTextSpanElem = document.createElement("span");
		moreTextSpanElem.id = `more-${uniqueId}`;
		moreTextSpanElem.setAttribute("style", "display: none;  white-space: normal;");

		const toggleButtonElem = document.createElement("li");
		toggleButtonElem.id = `myBtn-${targetId}`;
		toggleButtonElem.onclick = () => toggle(targetId);
		toggleButtonElem.setAttribute(
		  "style",
		  "text-decoration: underline; cursor: pointer;"
		);
		toggleButtonElem.innerHTML = wordText;

		const spaceSpanElem = document.createElement("span");
		spaceSpanElem.textContent = " ";
		spaceSpanElem.setAttribute("style", "display: inline");

		const playButtonElem = document.createElement("i");
		playButtonElem.id = `myPlay-${uniqueId}`;
		playButtonElem.onclick = () => playTranslation(`${uniqueId}`);
		playButtonElem.setAttribute(
		  "class",
		  "fa fa-play"
		);

		const firstText = paragraphText.slice(0, displayLimit);
		const secondText = paragraphText.slice(displayLimit);
		lessTextSpanElem.innerHTML = firstText;
		moreTextSpanElem.innerHTML = secondText;

		pTagElem.appendChild(toggleButtonElem);
		pTagElem.appendChild(playButtonElem);
		pTagElem.appendChild(spaceSpanElem);
		pTagElem.appendChild(lessTextSpanElem);
		pTagElem.appendChild(dotsSpanElem);
		pTagElem.appendChild(moreTextSpanElem);
		document.getElementById(targetId) ?
		document.getElementById(targetId).appendChild(pTagElem) :
		document.body.appendChild(pTagElem);
	};

</script>

