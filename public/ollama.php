<?php

	header('Content-Type: application/json');

	// Get the POST data
	$postData = json_decode(file_get_contents('php://input'), true);
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, "http://localhost:11434/api/generate");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
		'prompt' => $postData['prompt'],
		'model' => "granite3-moe:3b",
		'keep_alive' => "30m"
	]));
	curl_setopt($ch, CURLOPT_WRITEFUNCTION, function($ch, $data) {
		$chunc = json_decode($data, true);
		if ($chunc['done'] == false) {
			echo $chunc['response'];
		}
		ob_flush();
		flush();
		return strlen($data);
	});

	$response = curl_exec($ch);

	if ($response === false) {
		http_response_code(500);
		echo 'Error: ' . curl_error($ch);
	}

	curl_close($ch);
