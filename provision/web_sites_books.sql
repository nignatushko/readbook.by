COPY public.web_site (id, name) FROM stdin;
1	lib.ru
2	gutenberg.org
3	lib.pravmir.ru
4	readbook.by
\.

COPY public.web_book (id, web_site_id, book_name, book_url, author_name, book_genre) FROM stdin;
71480	1	Преступление и наказание	http://az.lib.ru/d/dostoewskij_f_m/text_0060.shtml	Достоевский Федор Михайлович	Русская классика
146971	3	Лето Господне	https://lib.pravmir.ru/library/readbook/1381	Шмелёв Иван Сергеевич	
92456	2	Moby Dick; Or, The Whale	$(curl -s https://www.gutenberg.org/ebooks/2489 | grep 'title="Download".Plain Text' | sed 's/^.*href="/https:\\/\\/www.gutenberg.org/' | sed 's/".*//')	Melville, Herman, 1819-1891	Whaling -- Fiction; Sea stories; Psychological fiction; Ship captains -- Fiction; Adventure stories; Mentally ill -- Fiction; Ahab, Captain (Fictitious character) -- Fiction; Whales -- Fiction; Whaling ships -- Fiction
153275	4	Библия. Синодальный перевод.	https://readbook.by/bible_synod.txt	Слово Божье	Религиозная литература
\.
