<?php

namespace App\Command;

use App\Entity\WebBook;
use App\Entity\WebSite;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\DBAL\Connection;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Input\InputArgument;

#[AsCommand(
    name: 'app:libru-update',
    description: 'Update books list of Lib.ru.',
    hidden: false
)]
class LibRuUpdate extends Command
{
    //private Connection $connection;
    private $webBookRepo;
    private $webSiteRepo;
    private $entityManager;

    public function __construct(Connection $connection, ManagerRegistry $doctrine)
    {
        //$this->connection = $connection;
        $this->webBookRepo = $doctrine->getRepository(WebBook::class);
        $this->webSiteRepo = $doctrine->getRepository(WebSite::class);
        $this->entityManager = $doctrine->getManager();

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $seactionName = $input->getArgument('name');
        $section = $input->getArgument('url');
        $time_interval = $input->getArgument('interval');

        $html = $this->getHtmlPage($section, $time_interval);
        preg_match_all("/<li>.*<a href=.*lib.ru.*>/i", $html, $links_author_www);
        preg_match_all("/<b>dir<\/b>.*<A HREF.*<\/b>/i", $html, $links_author_dir);

        $books_count = 0;
        $libRuSite = $this->webSiteRepo->findOneBy(['Name' => 'lib.ru']);

        if ($libRuSite === null){
            return Command::FAILURE;
        }

        $libRuBooks = $libRuSite->getWebBooks()->filter(function($web_book) use ($seactionName) {
            return $web_book->getBookGenre() === $seactionName;
        });

        if (!empty($libRuBooks)){
            foreach ($libRuBooks as $book) {
                $output->writeln("Deleting " . $book->getBookName());
                $libRuSite->removeWebBook($book);
                $this->entityManager->remove($book);
            }
        }
       
        foreach ($links_author_www[0] as $str){
            $temp = explode("<A HREF=", $str)[1]; 
            $temp = explode("><b>", $temp);
            $authorUrl = trim($temp[0]);
            $authorName = trim(explode("</b></A>", $temp[1])[0]);

            $html = $this->getHtmlPage($authorUrl, $time_interval);

            preg_match_all("/<DL><DT><li><A HREF.*\.shtml.*<\/b>/i", $html, $links_book_shtml);
            
            foreach ($links_book_shtml[0] as $bookShtml){
                $temp = explode("</b>", $bookShtml)[0];
                $temp = explode("<b>", $temp);
                $bookName = trim($temp[1]);
                $bookUrl = trim(substr($temp[0], 20, -1));

                $output->writeln("$books_count : $bookName - $authorName - $authorUrl$bookUrl");
                $this->createWebBook($bookName, $authorName, $authorUrl, $bookUrl, $libRuSite, $seactionName);

                $books_count++;
            }
        }

        foreach ($links_author_dir[0] as $str){
            $temp = explode("<A HREF=", $str)[1];
            $temp = explode("><b>", $temp);
            $authorUrl = trim($section . $temp[0]);
            $authorName = trim(substr($temp[1], 0, -4));

            $html = $this->getHtmlPage($authorUrl, $time_interval);
            preg_match_all("/<A HREF.*\.txt.*<\/b><\/A>/i", $html, $links_book_txt);
            

            foreach ($links_book_txt[0] as $bookTxt){
                $parts = explode('<A', $bookTxt);
                if (isset($parts[2])) {
                    $temp = $parts[2];
                    $temp = explode('<b>', $temp);
                    $bookUrl = trim(substr($temp[0], 6, -1));
                    $bookName = trim(substr($temp[1], 0, -8));

                    $output->writeln("$books_count : $bookName - $authorName - $authorUrl$bookUrl");
                    $this->createWebBook($bookName, $authorName, $authorUrl, $bookUrl, $libRuSite, $seactionName);

                    $books_count++;
                }
            }
        }

        $this->entityManager->flush();
        return Command::SUCCESS;
    }

    private function getHtmlPage($url, $time_interval) 
    {
        sleep($time_interval);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, true);
        $out = curl_exec($ch);
        preg_match_all('/(?<=charset=).*?(?=\s)/i', $out, $charset);
        $out = iconv($charset[0][0], 'UTF-8', $out);
        
        return $out;
    }

    private function createWebBook($bookName, $authorName, $authorUrl, $bookUrl, $libRuSite, $genre) {
        $webBook = new WebBook;
        $webBook->setBookName($bookName);
        $webBook->setAuthorName($authorName);
        $webBook->setBookUrl($authorUrl . $bookUrl);
        $webBook->setWebSite($libRuSite);
        $webBook->setBookGenre($genre);
        $this->entityManager->persist($webBook);
    }

    protected function configure(): void
    {
        $this
            ->addArgument('name', InputArgument::REQUIRED, 'Genre name of the Lib.ru.')
            ->addArgument('url', InputArgument::REQUIRED, 'Genre url of the Lib.ru.')
            ->addArgument('interval', InputArgument::REQUIRED, 'Time interval between requests.')
        ;
    }
}
