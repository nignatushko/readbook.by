<?php

namespace App\Command;

use App\Entity\WebSite;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Doctrine\Persistence\ManagerRegistry;

#[AsCommand(
    name: 'app:website-update',
    description: 'Add website',
    hidden: false
)]
class WebSitesUpdate extends Command
{
    private $entityManager;
    private array $webSites;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->entityManager = $doctrine->getManager();
        $this->webSites = ['Lib.ru'];

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->webSites as $webSite) {
            $res = $this->addWebSite($webSite, $output);
            if ($res) {
                return Command::SUCCESS;
            } else {
                return Command::INVALID;
            }
        }        
    }

    private function addWebSite($name, OutputInterface $output): bool
    {
        if (strlen($name) > 1) {
            $webSite = new WebSite;
            $webSite->setName($name);
            $this->entityManager->persist($webSite);
            $this->entityManager->flush();
            $output->writeln("Website with name '$name' created");
            return true;
        }
        else {
            return false;
        }
    }
}