<?php

namespace App\Form;

use App\Entity\Book;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookSettingsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('speed', RangeType::class, [
                'attr' => [
                    'min' => -10,
                    'max' => 10,
                    'value' => $options['currentSpeed'],
                ],
            ])
            ->add('voice', ChoiceType::class, [
                'choices'  => [
                    'Русский - Александр' => 'aleksandr',
                    'Русский - Анна' => 'anna',
                    'Русский - Артемий' => 'artemiy',
                    'Русский - Елена' => 'elena',
                    'Русский - Ирина' => 'irina',
                    'English - Alan' => 'alan',
                    'English - Bdl' => 'bdl',
                    'English - Clb' => 'clb',
                    'English - Slt' => 'slt',
                ],
                'attr' => [
                    'value' => $options['currentVoice'],  
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
            'validation_groups' => false,
            'currentVoice' => 'anna',
            'currentSpeed' => 1,
        ]);
    }
    
}
