<?php

namespace App\Form;

use App\Entity\Book;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookProgressFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('progress', RangeType::class, [
            'attr' => [
                'min' => 1,
                'max' => $options['maxProgress'],
                'value' => $options['currentProgress'],
            ],
            'mapped' => false,
        ]);
    }
    
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
            'currentProgress' => 1,
            'maxProgress' => 100,
        ]);
    }
}