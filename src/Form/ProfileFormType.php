<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\User;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('lang', ChoiceType::class, [
                'choices'  => [
                    'Русский' => 'ru',
                    'English' => 'en',
                ],
            ])
            ->add('PanelPosition', ChoiceType::class, [
                'choices'  => [
                    $options['positionRightName'] => 'Right',
                    $options['positionLeftName'] => 'Left',
                    $options['positionHideName'] => 'Hide',
                ],
            ])
        ;
        
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'position' => 'Right',
            'positionRightName' => 'Право',
            'positionLeftName' => 'Лево',
            'positionHideName' => 'Спрятать',
        ]);
    }
}