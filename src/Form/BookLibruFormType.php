<?php

namespace App\Form;

use App\Entity\Book;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class BookLibruFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('searchBook', TextType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('selectBook', ChoiceType::class, [
                'attr' => ['size' => '15'], 
                'mapped' => false,
            ]);
        
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $parentChoice = $event->getData();
    
            $event->getForm()->add('selectBook', ChoiceType::class, [
                'attr' => ['size' => '15'], 
                'mapped' => false,
                'choices' => $parentChoice,
            ]);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
            'validation_groups' => false,
        ]);
    }
    
}