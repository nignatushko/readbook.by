<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;
use Symfony\Component\HttpKernel\KernelInterface;

class FilesService
{
    private string $app_path;
    private Filesystem $filesystem;

    public function __construct(KernelInterface $kernel)
    {
        $this->app_path = $kernel->getProjectDir();
        $this->filesystem = new Filesystem();
    }

    public function addBook(string $text, string $user_dir): string
    {
        $path = Path::makeAbsolute("book_storage/$user_dir", $this->app_path);
        do
        {
            $fileName = $path . '/' . $this->generateRandomString() . ".txt.gz";
        } while($this->filesystem->exists($fileName));
        $this->overwriteBook($fileName, $text);

        return $fileName;
    }

    public function addFolderForNewUser(string $name): ?bool
    {
        $full_path = $this->app_path . '/book_storage/' . $name;
        if (!$this->filesystem->exists($full_path)) {
            $this->filesystem->mkdir($full_path);
            return true;
        }
        else
            return false;
    }

    public function readUserBook(string $path): bool|string
    {
        return gzdecode(file_get_contents($path));
    }

    public function deleteBook(string $path): void
    {
        $this->filesystem->remove($path);
    }

    public function overwriteBook(string $path, $text): void
    {
        $this->filesystem->dumpFile($path, gzencode($text));
    }

}