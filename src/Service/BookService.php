<?php

namespace App\Service;

use Doctrine\DBAL\Connection;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BookService {
    const DOT_ABBREVATIONS = array(
        'Dr.' => ['readbook_doctor', '\bDr\.'],
        'Mr.' => ['readbook_mister', '\bMr\.'],
        'Mrs.' => ['readbook_missis', '\bMrs\.'],
        'Ms.' => ['readbook_young_miss', '\bMs\.'],
        'M.' => ['readbook_musee', '\bMs\.']
    );
    private Connection $readbooks_connection;
 
    private Connection $main_connection;

    public function __construct(Connection $readbooks_connection, Connection $main_connection)
    {
        $this->readbooks_connection = $readbooks_connection;
        $this->main_connection = $main_connection;
    }

    private function dotAbbrevetaionsEncode($text) {
        foreach ($this::DOT_ABBREVATIONS as $key => $value) {
            $text = preg_replace('/' . $value[1] . '/', $value[0], $text);
        }
        return $text;
    }

    private function dotAbbrevetaionsDecode($text) {
        foreach ($this::DOT_ABBREVATIONS as $key => $value) {
            $text = preg_replace('/' . $value[0] . '/', $key, $text);
        }
        return $text;
    }

    public function createBook($text, $replace = false, $replaceName = '')
    {
        $text = $this->dotAbbrevetaionsEncode($text);
        $spans = preg_split("/([.!?:;])/", $text, -1, PREG_SPLIT_DELIM_CAPTURE);
        $final = [$spans[0]];
        for ($i = 1; $i < count($spans); $i++) {
            if (strlen($spans[$i]) < 10 && strlen($final[count($final)-1]) < 500) {
                $final[count($final)-1] .=  $spans[$i];
            } else {
                array_push($final, $spans[$i]);
            }
        }
        $maxLenSpan = max(array_map('strlen', $final));

        if (!$replace){
            $tableName = $this->generateRandomString();
            while (!$this->checkFreeName($tableName)) {
                $tableName = $this->generateRandomString();
            }
        }
        else {
            $tableName = $replaceName;
        }

        $this->readbooks_connection->fetchAllAssociative("CREATE TABLE $tableName (id serial PRIMARY KEY, span VARCHAR ( $maxLenSpan ), lastupdate timestamp)");

        foreach ($final as $key => $value) {
            $final[$key] = $this->dotAbbrevetaionsDecode($final[$key]);
        }

        $clearSpans = preg_replace('/[<>]/', '', implode("'),('", array_map("pg_escape_string", $final)));

        $this->readbooks_connection->fetchAllAssociative("INSERT INTO $tableName (span) VALUES ('" . $clearSpans . "')");


        return $tableName;
    }

    public function getSpans($tableName, $startPoint, $endPoint) 
    {
        return $this->readbooks_connection->fetchAllAssociative("SELECT id, REGEXP_REPLACE(span, '([a-zA-Zа-яА-Я, ])[\n\r]{1,2}', '\\1 ', 'g')::text as span FROM " . 
                                                                 $tableName . 
                                                                 " WHERE id BETWEEN " . 
                                                                 (int)$startPoint . 
                                                                 " AND " .
                                                                 (int)$endPoint .
                                                                 " ORDER BY id"
        );
    }

    public function getPlainText($tableName)
    {
        $spans = $this->readbooks_connection->fetchAllAssociative("SELECT span FROM " . $tableName . " ORDER BY id");
        $text = "";
        for ($i = 0; $i < count($spans); $i++) {
            $text .= $spans[$i]['span'];
        }

        return $text;
    }

    public function getSpansCount($tableName)
    {
        return $this->readbooks_connection->fetchAllAssociative("SELECT count(span) FROM " . $tableName);
    }

    public function getLanguageByContent($tableName)
    {
        return $this->readbooks_connection->fetchAllAssociative(
            "
                WITH a AS (
                    SELECT
                        regexp_split_to_table(span, '') AS o,
                        count(1)
                    FROM
                        $tableName AS t1
                    GROUP BY
                        o
                    ORDER BY
                        2 DESC
                    LIMIT 300
                )
                SELECT
                    CASE
                        WHEN substring(lower(o) FROM '[a-z]') is not null THEN 'en'
                        WHEN substring(lower(o) FROM '[а-я]') is not null THEN 'ru'
                    END as lang
                FROM
                    a
                WHERE
                    o ~* '[[:alpha:]]'
                LIMIT 1
            ");
    }

    public function getWordsByDate($tableName)
    {
        return $this->readbooks_connection->fetchAllAssociative("
            WITH total_words AS (
                SELECT array_length(regexp_split_to_array(string_agg(span, ' '), ' '), 1) as total_words_count
                  FROM " . $tableName . "
            ),
            words_by_date AS (
                SELECT TO_CHAR(lastupdate, 'yyyy-mm-dd') as span_lastupdate,
                       array_length(regexp_split_to_array(string_agg(span, ' '), ' '), 1) as words_count
                  FROM " . $tableName . " as t1
                 WHERE lastupdate is not null
                 GROUP BY TO_CHAR(lastupdate, 'yyyy-mm-dd') 
                 ORDER BY 1 desc
            )
            SELECT t1.span_lastupdate,
                   t1.words_count,
                   round(100::numeric * t1.words_count::numeric / t2.total_words_count::numeric, 2) as perc
              FROM words_by_date AS t1
                   INNER JOIN total_words AS t2 ON 1 = 1 
                 ORDER BY 1 desc
         ");
    }

    public function deleteBook($tableName)
    {
        return $this->readbooks_connection->fetchAllAssociative("DROP TABLE " . $tableName);
    }

    public function getTextFromURL(string $url): string
    {
        $path = '/tmp/' . $this->generateRandomString();
        passthru("curl -q -L -o $path $url");
        return $this->getTextFromFile(new UploadedFile($path, basename($path), mime_content_type($path)));
    }

    public function getTextFromFile(UploadedFile $file): string
    {
		$type = $file->getClientMimeType();
		//die($type);
		switch ($type) {
			case 'text/html':
				ob_start();
				system('cat "' . $file->getPathname() . '"  | DEFAULT_CHARSET=UTF-8 enconv -L ru | LANG=en_US.utf8 w3m -T text/html 2>&1');
				$text = ob_get_contents();
				ob_end_clean();
				break;
			case 'text/plain':
				ob_start();
				system('cat "' . $file->getPathname() . '"  | DEFAULT_CHARSET=UTF-8 enconv -L ru');
				$text = ob_get_contents();
				ob_end_clean();
				break;
			case 'application/pdf':
				ob_start();
				system('pdftotext -layout "' . $file->getPathname() . '" - | sed "s///g"  2>&1 | DEFAULT_CHARSET=UTF-8 enconv -L ru');
				$text = ob_get_contents();
				ob_end_clean();
				break;
			case 'application/x-fictionbook+xml':
				ob_start();
				system('cat "' . $file->getPathname() . '" | DEFAULT_CHARSET=UTF-8 enconv -L ru 2>&1 | pandoc -f fb2 -tplain -o /dev/stdout 2>&1');
				$text = ob_get_contents();
				ob_end_clean();
				break;
			case 'application/epub+zip':
				ob_start();
				system('pandoc "' . $file->getPathname() . '" -f epub -tplain -o /dev/stdout 2>&1 | DEFAULT_CHARSET=UTF-8 enconv -L ru 2>&1');
				$text = ob_get_contents();
				ob_end_clean();
				break;
			case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
				ob_start();
				system('pandoc "' . $file->getPathname() . '" -f docx -tplain -o /dev/stdout 2>&1 | DEFAULT_CHARSET=UTF-8 enconv -L ru 2>&1');
				$text = ob_get_contents();
				ob_end_clean();
				break;
			case 'application/msword':
				ob_start();
				system('LANG=en_US.utf8 antiword "' . $file->getPathname() . '" 2>&1 | DEFAULT_CHARSET=UTF-8 enconv -L ru 2>&1');
				$text = ob_get_contents();
				ob_end_clean();
				break;
			case 'application/vnd.oasis.opendocument.text':
				ob_start();
				system('pandoc "' . $file->getPathname() . '" -f odt -tplain -o /dev/stdout 2>&1 | DEFAULT_CHARSET=UTF-8 enconv -L ru 2>&1');
				$text = ob_get_contents();
				ob_end_clean();
				break;
			default:
				ob_start();
				system('cat "' . $file->getPathname() . '"  | DEFAULT_CHARSET=UTF-8 enconv -L ru');
				$text = ob_get_contents();
				ob_end_clean();
				break;
		}
		return $text;
    }

    public function getWebSites($em)
    {
        $qb = $em->createQueryBuilder();
        $qb->select('s.Name')
            ->from('App\Entity\WebSite', 's');
        $items = $qb->getQuery()->execute();
        $output = [];
        foreach ($items as $item) {
            $output[] = $item['Name'];
        }
        return $output;
    }

    public function getLibruBooks($em, $searchBook)
    {
        $func = function($word) { return "%" . mb_strtolower($word) . "%"; };
        $words = array_map($func, explode(" ", $searchBook));

        $qb = $em->createQueryBuilder();
        $qb->select('w.BookName, w.AuthorName, w.BookUrl')
            ->from('App\Entity\WebBook', 'w')
            ->setMaxResults(100);

        foreach ($words as $key => $word) {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->like("lower(w.BookName)", "?$key"),  
                $qb->expr()->like("lower(w.AuthorName)", "?$key"),
            ))
            ->setParameter($key, $word);
        }
        
        return $qb->getQuery()->execute();
    }

    public function getWordsCount(string $tableName, int $point = 0)
    {
        $allRowsCount = $this->getSpansCount($tableName)[0]['count'];
        if ($allRowsCount !== 0) {
            if ($point < 1 || $point > $allRowsCount) {
                $point = $allRowsCount;
            }
            
            $rows = $this->readbooks_connection->fetchAllAssociative("SELECT id, span FROM " . 
                                                                      $tableName .
                                                                      " LIMIT " . 
                                                                      (int)$point);
            $wordsCount = 0;
            foreach ($rows as $row) {
                preg_match_all('/\s+/i', $row['span'], $matches);
                $wordsCount += count($matches[0]);
            }
            
            return $wordsCount;
        } else {
            throw new Exception("Wrong table");
        }
    }

    public function getSpanByteSize(string $tableName, int $point = 0)
    {
        $allRowsCount = $this->getSpansCount($tableName)[0]['count'];
        if ($allRowsCount !== 0) {
            if ($point < 1 || $point > $allRowsCount) {
                $point = $allRowsCount;
            }
            
            $rows = $this->readbooks_connection->fetchAllAssociative("SELECT id, span FROM " .
                                                                      $tableName .
                                                                      " LIMIT " . 
                                                                      (int)$point);
            $byteSize = 0;
            foreach ($rows as $row) {
                $byteSize += mb_strlen($row['span'], '8bit');
            }
            
            return round($byteSize / 1024, 2);
        } else {
            throw new Exception("Wrong table");
        }
    }

    public function setSpanTimestamp(string $tableName, int $id)
    {
        $this->readbooks_connection->fetchAllAssociative("
            UPDATE " .  $tableName . "
               SET lastupdate = now()
             WHERE id = " . (int)$id . "
        ");
    }

    private function generateRandomString($length = 16, $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') : string
    {
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    private function checkFreeName($tableName) : bool
    {
        $books = $this->readbooks_connection->fetchAllAssociative("SELECT table_name 
                                                                   FROM information_schema.tables 
                                                                   WHERE table_schema = 'public'");
        for ($i = 0; $i < count($books); $i++) {
            if ($books[$i]['table_name'] === $tableName) {
                return false;
            }     
        }

        return true;
    }
}
