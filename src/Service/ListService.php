<?php

namespace App\Service;

use Doctrine\DBAL\Connection;

class ListService 
{
    public function search(Connection $connection, $tableName, $query, $idColName, 
                           $targetColName, $page = 1, $rowCount = 10, $disableHighlight = false) {
        $response = [];

        if (preg_match('/[А-Яа-яЁё]/u', $query)){
            $lang = 'russian';
        }
        else {
            $lang = 'english';
        }

        $query = str_replace(" ", "&", $query);

        $startRow = ($page - 1) * $rowCount;

        $pageCount = $connection->fetchAllAssociative("
            SELECT count($targetColName) FROM $tableName 
            WHERE to_tsvector('$lang', $tableName.$targetColName) @@ plainto_tsquery('$lang', '$query')")[0]["count"];
        $pageCount = ceil($pageCount / $rowCount);

        $pagePad = [];

        $startPage = $page - 2 > 0 ? $page - 2 : 1;
        $endPage = $page + 2 < $pageCount ? $page + 2 : $pageCount;

        if ($startPage !== 1) {
            $pagePad["<<"] = 1;

        }

        if ($page > 1) {
            $pagePad["<"] = $page - 1;
        }

        for ($i = $startPage; $i <= $endPage; $i++) {
            if ($i === $page) {
                $pagePad["current"] = $i;   
            }
            else {
                $pagePad["$i"] = $i;
            }
        }

        if ($page < $pageCount) {
            $pagePad[">"] = $page + 1;
        }

        if ($endPage !== $pageCount) {
            $pagePad[">>"] = $pageCount;
        }
        
        $response['pagePad'] = $pagePad; 

        if ($disableHighlight) {
            $select = "SELECT $idColName, $tableName.$targetColName AS ts_headline ";
        }
        else {
            $select = "SELECT $idColName, ts_headline('$lang', $tableName.$targetColName , plainto_tsquery('$lang', '$query'), 'HighlightAll=TRUE') ";
        }
        $response['searches'] = $connection->fetchAllAssociative(
            $select .
            "FROM $tableName
            WHERE to_tsvector('$lang', $tableName.$targetColName) @@ plainto_tsquery('$lang', '$query')
            ORDER BY ts_rank(to_tsvector('$lang', $tableName.$targetColName), plainto_tsquery('$lang', '$query')) DESC
            LIMIT $rowCount
            OFFSET $startRow;");

        return $response;
    }
}