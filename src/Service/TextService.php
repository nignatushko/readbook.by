<?php

namespace App\Service;

use App\Entity\Book;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpKernel\KernelInterface;

class TextService
{
    private string $app_path;
    private \Doctrine\Persistence\ObjectManager $entityManager;
    private \Doctrine\Persistence\ObjectRepository $bookRepository;
    private Connection $connection;

    public function __construct(KernelInterface $kernel, ManagerRegistry $doctrine, Connection $connection)
    {
        $this->app_path = $kernel->getProjectDir();
        $this->entityManager = $doctrine->getManager();
        $this->bookRepository = $doctrine->getRepository(Book::class);
        $this->connection = $connection;
    }

    public function textToSpeech(string $text, string $voice, string $speed): string
    {
        ob_start();
        $clear_text = escapeshellarg($text);
        if (!preg_replace("/[^\w$\x{0080}-\x{FFFF}]+/u", '', $clear_text)) {
            $clear_text = "um"; 
        }
        $com = "echo $clear_text | $this->app_path/tts.sh -r $speed -s $voice";
        passthru($com);
        $speech = ob_get_contents();
        ob_end_clean();

        return $speech;
    }

    public function saveVars($user, $bookId, ...$vars): bool
    {
        //$currentPoint, $voice, $speed
        $book =$this->bookRepository->find($bookId);
        if ($user === $book->getOwner()) {
            $book->setCurrentPoint($vars[0]);
            $this->entityManager->flush();
            return true;
        }
        else return false;
    }

    public function getVars($user, $bookId): array
    {
        $book =$this->bookRepository->find($bookId);
        $out = [];
        if ($user === $book->getOwner()) {
            $out['currentPoint'] = $book->getCurrentPoint();
            $out['voice'] = $book->getVoice();
            $out['speed'] = $book->getSpeed();
        }

        return $out;
    }

    /**
     * @throws Exception
     */
    public function getWordTranslate($word): array
    {
        return $this->connection->fetchAllAssociative(
            "
					WITH q AS (
						SELECT plainto_tsquery('" . pg_escape_string($word) . "') AS f
					)
					SELECT ts_headline(muller.word, f, 'HighlightAll=TRUE')
					  FROM muller
						   INNER JOIN muller_vector ON muller.id = muller_vector.id
						   INNER JOIN q ON 1 = 1
					 WHERE word_vector @@ f
					   and SUBSTRING(word FROM 1 FOR 2) ILIKE SUBSTRING('" . pg_escape_string($word) . "' FROM 1 FOR 2)
					 ORDER BY CASE WHEN SPLIT_PART(word, ' ', 1) ILIKE '" . pg_escape_string($word) . "' THEN 0 ELSE 1 END, word
				"
        );
    }
    
}

