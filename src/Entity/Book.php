<?php

namespace App\Entity;

use App\Repository\BookRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: BookRepository::class)]
class Book
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
	#[Assert\Length(min: 1, max: 255,)]
    private ?string $Name = null;

    #[ORM\Column(nullable: true)]
    private ?int $CurrentPoint = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $FilePath = null;

    #[ORM\ManyToOne(inversedBy: 'Books')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $Owner = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $voice = null;

    #[ORM\Column(nullable: true)]
    private ?int $speed = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $LastDate = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = substr($Name, 0, 255);

        return $this;
    }

    public function getCurrentPoint(): ?int
    {
        return $this->CurrentPoint;
    }

    public function setCurrentPoint(?int $CurrentPoint): self
    {
        $this->CurrentPoint = $CurrentPoint;

        return $this;
    }

    public function getFilePath(): ?string
    {
        return $this->FilePath;
    }

    public function setFilePath(?string $FilePath): self
    {
        $this->FilePath = substr($FilePath, 0, 255);

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->Owner;
    }

    public function setOwner(?User $Owner): self
    {
        $this->Owner = $Owner;

        return $this;
    }

    public function getVoice(): ?string
    {
        return $this->voice;
    }

    public function setVoice(?string $voice): self
    {
        $this->voice = substr($voice, 0, 50);

        return $this;
    }

    public function getSpeed(): ?int
    {
        return $this->speed;
    }

    public function setSpeed(?int $speed): self
    {
        $this->speed = $speed;

        return $this;
    }

    public function getLastDate(): ?\DateTimeInterface
    {
        return $this->LastDate;
    }

    public function setLastDate(\DateTimeInterface $LastDate): self
    {
        $this->LastDate = $LastDate;

        return $this;
    }
}
