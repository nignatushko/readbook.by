<?php

namespace App\Entity;

use App\Repository\WebSiteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WebSiteRepository::class)]
class WebSite
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $Name = null;

    #[ORM\OneToMany(mappedBy: 'WebSite', targetEntity: WebBook::class)]
    private Collection $webBooks;

    public function __construct()
    {
        $this->webBooks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    /**
     * @return Collection<int, WebBook>
     */
    public function getWebBooks(): Collection
    {
        return $this->webBooks;
    }

    public function addWebBook(WebBook $webBook): self
    {
        if (!$this->webBooks->contains($webBook)) {
            $this->webBooks->add($webBook);
            $webBook->setWebSite($this);
        }

        return $this;
    }

    public function removeWebBook(WebBook $webBook): self
    {
        if ($this->webBooks->removeElement($webBook)) {
            // set the owning side to null (unless already changed)
            if ($webBook->getWebSite() === $this) {
                $webBook->setWebSite(null);
            }
        }

        return $this;
    }
}
