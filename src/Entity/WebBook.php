<?php

namespace App\Entity;

use App\Repository\WebBookRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WebBookRepository::class)]
class WebBook
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 1000)]
    private ?string $BookName = null;

    #[ORM\ManyToOne(inversedBy: 'webBooks')]
    private ?WebSite $WebSite = null;

    #[ORM\Column(length: 1000)]
    private ?string $BookUrl = null;

    #[ORM\Column(length: 1000)]
    private ?string $AuthorName = null;

    #[ORM\Column(length: 75)]
    private ?string $BookGenre = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBookName(): ?string
    {
        return $this->BookName;
    }

    public function setBookName(string $BookName): self
    {
        $this->BookName = $BookName;

        return $this;
    }

    public function getWebSite(): ?WebSite
    {
        return $this->WebSite;
    }

    public function setWebSite(?WebSite $WebSite): self
    {
        $this->WebSite = $WebSite;

        return $this;
    }

    public function getBookUrl(): ?string
    {
        return $this->BookUrl;
    }

    public function setBookUrl(string $BookUrl): self
    {
        $this->BookUrl = $BookUrl;

        return $this;
    }

    public function getAuthorName(): ?string
    {
        return $this->AuthorName;
    }

    public function setAuthorName(string $AuthorName): self
    {
        $this->AuthorName = $AuthorName;

        return $this;
    }

    public function getBookGenre(): ?string
    {
        return $this->BookGenre;
    }

    public function setBookGenre(string $BookGenre): self
    {
        $this->BookGenre = $BookGenre;

        return $this;
    }
}
