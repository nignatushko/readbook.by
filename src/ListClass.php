<?php

namespace App;

use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\Request;

class ListClass 
{
    private Connection $connection;
    private array $columns;
    private array $searchColumns;
    private string $searchName;
    private int $limit;
    private int $page;
    private string $urlController;
    private string $sql;
    private string $urlAction;
    private string $actionColName;
    private array $actionParameters;
    private string $actionName;
    private bool $resultCount;
    private Request $request;

    public function __construct(string $urlController, Connection $connection, string $sql, 
                                Request $request, int $limit = 24)
    {
        $this->connection = $connection;
        $this->request = $request;
        $this->page = (int)$this->request->query->get('page') ?: 1;
        $this->limit = $limit;
        $this->sql = $sql;
        $this->urlController = $urlController;
        $this->columns = [];
        $this->urlAction = '';
        $this->actionParameters = [];
        $this->actionName = '';
        $this->searchColumns = [];
        $this->searchName = 'Search';
        $this->actionColName = 'Action';
    }

    public function addColumn(string $columnName, string $prettyName, string $changeParameter = '',
                                                                      string $sqlColOutput = '')
    {
        $this->columns[$columnName] = ['prettyName' => $prettyName, 
                                       'changeParameter' => $changeParameter,
                                       'sqlColOutput' => $sqlColOutput];

    }

    public function setAction(string $url, string $actionName, array $parameters = [])
    {
        $this->urlAction = $url;
        $this->actionName = $actionName;
        $this->actionParameters = $parameters;
    }

    public function addSearch(string $columnName, string $sqlField, string $parameter, bool $isFullText, array $selectParameters = [])
    {
        $this->searchColumns[$columnName] = [$sqlField, $parameter, $isFullText, $selectParameters]; 
    }

    public function setSearchName($searchName)
    {
        $this->searchName = $searchName;
    }

    public function setActionColName($actionColName)
    {
        $this->actionColName = $actionColName;
    }

    public function getHTML()
    {
        $response = [];
        $htmlAddSearchParams = '';

        if (!empty($this->searchColumns)) {
            $whereString = '';
            foreach ($this->searchColumns as $columnName =>  [$sqlField, $parameter, $isFullText]) {
                $parameter = pg_escape_string($parameter);
                if (!empty($parameter)) {

                    if ($isFullText && $this->isFullTextConfirmed($columnName)) {
                        $lang = $this->getLanguage($parameter);
                        $whereString .= " AND to_tsvector('$lang', $sqlField) @@ plainto_tsquery('$lang', '$parameter') ";
                    } else {
                        $whereString .= " AND $sqlField ~* '$parameter' ";
                    }

                    $htmlAddSearchParams .= "<input type='hidden' id='" . $columnName 
                                          . "' name='" . $columnName . "' value='" 
                                          . $parameter . "'>";
                    if ($isFullText) {
                        $htmlAddSearchParams .= "<input type='hidden' id='" . $columnName . "_chk' name='" . $columnName . "_chk'" .
                            " value='1' " . ($this->request->query->get($columnName . '_chk') == '1' ? 'checked' : '') . ">";
                    }
                }
            }
            $this->sql = str_replace('CHANGE_WHERE', $whereString, $this->sql);
        }


        $result = $this->connection->executeQuery($this->sql);
        $response['rowsCount'] = $result->rowCount();

        $pageCount = ceil($response['rowsCount'] / $this->limit);
        if ($this->page > $pageCount) {
            $this->page = 1;
        }

        $record_start  = ($this->page - 1) * $this->limit;
        $record_finish = $record_start + $this->limit;
        

        $htmlList = '<table><tr><th width="1%">#</th>';
        foreach ($this->columns as $columnName => $columnValues) {
            $htmlList .= '<th>' . $columnValues['prettyName'] . '</th>';
        }   
        if (!empty($this->urlAction)) {
            $htmlList .= '<th>' . $this->actionColName . '</th>';
        }
        $htmlList .= '</tr>';

        $i = 0;
        foreach ($result->iterateAssociative() as $row) {
            if ($i < $record_start) {
                $i++;
                continue;
            }
            if ($i > $record_finish) {
                $i++;
                break;
            }
            $i++;
            $htmlList .= '<tr>';
            $htmlList .= '<td>' . $i . '</td>';
            foreach ($this->columns as $columnName => $columnValues) {
                $target = $row[$columnName];
                if (!empty($columnValues['changeParameter']) && $this->isFullTextConfirmed($columnName)) {
                        if (!empty($columnValues['sqlColOutput'])) {
                            $lang = $this->getLanguage($this->searchColumns[$columnName][1]); 
                            $var = str_replace('CHANGE_LANG', $lang, $columnValues['changeParameter']);
                            $var = str_replace('CHANGE_TARGET', pg_escape_string($target), $var);
                            $var = str_replace('CHANGE_QUERY', pg_escape_string($this->searchColumns[$columnName][1]), $var);
                            $parameter = $this->connection->fetchAllAssociative($var)
                                                                                [0]
                                                                                [$columnValues['sqlColOutput']]; 
                        } else {
                            eval($columnValues['changeParameter']);
                        }
                } else {
                    foreach ($this->searchColumns as $columnName =>  [$sqlField, $out, $isFullText]) {
                        if ($out) {
                            $target = preg_replace("/$out/ius", "<b>\\0</b>", $target);
                        }
                    }
                    $parameter = $target;
                }
                $parameter = preg_replace("/<b>/", "<b style=\"background-color: #FFFDD0;\">", $parameter);
                $htmlList .= '<td>' . $parameter . '</td>';
            }
            if (!empty($this->urlAction)) {
                $htmlList .= "<td>";
                foreach ($this->actionParameters as $parameter) {
                    $htmlList .= "<button onclick=\"location = '" . $this->urlAction . "?" . $parameter . "=" . $row[$parameter] . "'; return false;\">" . $this->actionName . "</button>";
                }
                $htmlList .= "</td>";
            }
            $htmlList .= '</tr>';
        }

        $htmlList .= '</table>';

        $response['htmlList'] = $htmlList;

        $startPage = $this->page - 4 > 0 ? $this->page - 4 : 1;
        $endPage = $this->page + 4 < $pageCount ? $this->page + 4 : $pageCount;

        $htmlPages = "<div class='list-form-pages'><input type='hidden' id='page' name='page' value='" . $this->page . "'>";
        if ($startPage !== 1) {
            $htmlPages .= "<button onclick='document.querySelector(\"#page\").value = 1; document.querySelector(\"#mainForm\").submit();'>1 <<</button>";
        }

        if ($this->page > 1) {
            $htmlPages .= "<button onclick='document.querySelector(\"#page\").value = " . $this->page - 1 . "; document.querySelector(\"#mainForm\").submit();'><</button>";
        }

        for ($i = $startPage; $i <= $endPage; $i++) {
            if ($i === $this->page) {
                $htmlPages .= "<button>" . $i . "*</button>";
            }
            else {
                $htmlPages .= "<button onclick='document.querySelector(\"#page\").value = " . $i . "; document.querySelector(\"#mainForm\").submit();'>" . $i . "</button>";
            }
        }

        if ($this->page < $pageCount) {
            $htmlPages .= "<button onclick='document.querySelector(\"#page\").value = " . $this->page + 1 . "; document.querySelector(\"#mainForm\").submit();'>></button>";
        }

        if ($endPage !== $pageCount) {
            $htmlPages .= "<button onclick='document.querySelector(\"#page\").value = " . $pageCount . "; document.querySelector(\"#mainForm\").submit();'>>> " . $pageCount . "</button>";
        }

        $htmlPages .= '</div></form>';

        $response['htmlPages'] = $htmlPages;

        if (!empty($this->searchColumns)){
            $htmlSearch = "<div class='list-form-search'><form id='mainForm' name='mainForm' action='" . $this->urlController . "'>";
            foreach ($this->searchColumns as $columnName => [$sqlField, $parameter, $isFullText, $selectParameters]) {
                if (!empty($selectParameters)) {
                    $htmlSearch .= "<select name='" . $columnName 
                                . "' value='" . htmlentities($parameter) 
                                . "' placeholder='" . $this->columns[$columnName]['prettyName'] ."'>";
                    $htmlSearch .= "<option value=''>-</option>";
                    foreach ($selectParameters as $select) {
                        $selected = $select == $parameter ? "selected" : "";
                        $htmlSearch .= "<option value='" . $select . "' " . $selected . ">" . $select . "</option>";
                    }
                    $htmlSearch .= "</select>";
                } else {
                    $htmlSearch .= "<div class='container-centered'>
                        <div><input type='text' id='" . $columnName . "' name='" . $columnName 
                    . "' value='" . htmlentities($parameter) 
                    . "' placeholder='" . $this->columns[$columnName]['prettyName'] ."' autofocus></div>";
                    if ($isFullText) {
                        $htmlSearch .= "<div><input title='Match Exactly' type='checkbox' id='" . $columnName . "_chk' name='" . $columnName . "_chk'" .
                            " value='1' " . ($this->request->query->get($columnName . '_chk') == '1' ? 'checked' : '') . "></div>";
                    }
                    $htmlSearch .= "</div>";
                }
            }
            $htmlSearch .= "<input type='submit' value='" . $this->searchName . "'></div>";
            $response['htmlSearch'] = $htmlSearch;
        }

        return $response;
    }

    private function getLanguage(string $parameter) 
    {
        if (preg_match('/[А-Яа-яЁё]/u', $parameter)){
            return 'russian';
        }
        else {
            return 'english';
        }
    }

    private function isFullTextConfirmed(string $parameter) 
    {
        return $this->request->query->get($parameter . '_chk') != '1';
    }
}
