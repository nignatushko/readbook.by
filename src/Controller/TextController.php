<?php

namespace App\Controller;

use App\Entity\Book;
use App\Service\BookService;
use App\Service\TextService;
use Doctrine\DBAL\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;

class TextController extends AbstractController
{
        
    #[Route('/text/get_audio', name: 'app_text_get_audio')]
    public function getAudio(Request $request, TextService $textService): Response
    {
        if ($this->getUser()) {
            $text = $request->query->get('text');
            $voice = $request->query->get('voice');
            $speed = $request->query->get('speed');
            $speech = $textService->textToSpeech($text, $voice, $speed);

            return new Response($speech, Response::HTTP_OK, ['content-Type' => 'audio/mpeg']);
        }
        else
            throw $this->createAccessDeniedException();
    }

    #[Route('/text/save_vars', name: 'app_text_save_vars')]
    public function saveVars(Request $request, TextService $textService, BookService $bookService, ManagerRegistry $doctrine)
    {
        if ($user = $this->getUser() ) {
            $bookId = $request->query->get('book_id');
            $currentPoint = $request->query->get('current_point');
            if (empty($bookId) or empty($currentPoint))
                $this->createAccessDeniedException();

            $textService->saveVars($user, intval($bookId), intval($currentPoint));

            $book = $doctrine->getRepository(Book::class)->find($bookId);
            $bookService->setSpanTimestamp($book->getFilePath(), $request->query->get('current_point'));

            return new Response();
        }
        else
            throw $this->createAccessDeniedException();
    }

    #[Route('/text/get_vars', name: 'app_text_get_vars')]
    public function getVars(Request $request, TextService $textService, BookService $bookService, ManagerRegistry $doctrine): JsonResponse
    {
        if ($user = $this->getUser()) {
            $bookId = $request->query->get('book_id');
            if (empty($bookId))
                $this->createAccessDeniedException();

            $vars = $textService->getVars($user, $bookId);
            if (count($vars) === 0)
                $this->createAccessDeniedException();

            $book = $doctrine->getRepository(Book::class)->find($bookId);
            $vars['spanCount'] = $bookService->getSpansCount($book->getFilePath());
            return new JsonResponse($vars);
        }
        else
            throw $this->createAccessDeniedException();

    }

    /**
     * @throws Exception
     */
    #[Route('/text/get_translate', name: 'app_text_translate')]
    public function getTranslate(Request $request, TextService $textService): JsonResponse
    {
        if ($user = $this->getUser()) {
            $text = $request->query->get('text');
            if (empty($text))
                $this->createAccessDeniedException();

            $words = preg_split("/[^\w]*([\s]+[^\w]*|$)/u", $text, -1, PREG_SPLIT_NO_EMPTY);
            $translatedWords = [];
            foreach ($words as $word) {
                if (strlen($word) > 1) {
                    $bodyTranslate = $textService->getWordTranslate($word);
                    if (!empty($bodyTranslate)) {
                        $translatedWords[] = ["name" => $word, "bodyTranslate" => $bodyTranslate];
                    } else {
                        $translatedWords[] = ["name" => $word, "bodyTranslate" => null];
                    }
                }
            }
            $response = new JsonResponse($translatedWords);
            $response->setEncodingOptions(JSON_UNESCAPED_UNICODE);

            return $response;
        }
        else
            throw $this->createAccessDeniedException();

    }
    
    
    #[Route('/text/get_spans', name: 'app_text_get_spans')]
    public function getAdditionalSpans(Request $request, BookService $bookService, ManagerRegistry $doctrine) 
    {
        $point = $request->query->get('point');
        $count = $request->query->get('count');
        $navigation = $request->query->get('navigation');
        $tableId = (int)$request->query->get('table_id');
        $bookId = $doctrine->getRepository(Book::class)->find($tableId);

        if ($bookId) {
            $bookName = $bookId->getFilePath();
        } else {
            throw $this->createNotFoundException();
        }

        if ($this->getUser() && $bookName) {
            try {
                if ($navigation === "1") {
                    $newSpans = $bookService->getSpans($bookName, $point, $point + $count);
                }
                else {
                    $newSpans = $bookService->getSpans($bookName, $point - $count, $point);
                }
            } catch (\Throwable $th) {
                return new JsonResponse([]);
            }  
        }
        else 
            throw $this->createAccessDeniedException();
        
        $response = new JsonResponse($newSpans);
        
        return $response;

    }
}
