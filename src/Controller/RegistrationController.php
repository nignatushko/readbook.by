<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Service\FilesService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class RegistrationController extends AbstractController
{
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    #[Route('/registration', name: 'app_registration')]
    public function index(Request $request, 
                          UserPasswordHasherInterface $userPasswordHasher, 
                          EntityManagerInterface $entityManager, 
                          FilesService $filesService,
                          TranslatorInterface $translator): Response
    {
        $user = new User();

        $auth_lang = $this->requestStack->getSession()->get('auth_lang');

        if ($auth_lang === 'ru' || $auth_lang === 'en') {
            $translator->setlocale($auth_lang);
            $user->setLang($auth_lang);
        } else {
            $user->setLang('ru');
        }

        $user->setPanelPosition('Right');

        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();

            $filesService->addFolderForNewUser($user->getId());

            return $this->redirectToRoute('app_book_index');
        }

        return $this->render('registration/index.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    #[Route('/registration/changeLang/{lang}', name: 'app_registration_change_lang')]
	public function changeLang(string $lang): Response
    {
        $this->requestStack->getSession()->set('auth_lang', $lang);

        return $this->redirectToRoute('app_registration');
    }
}