<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\Persistence\ManagerRegistry;
use App\Form\ProfileFormType;
use Symfony\Component\HttpFoundation\JsonResponse;


class ProfileController extends AbstractController
{
    private $entityManager;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->entityManager = $doctrine->getManager();
    }

    #[Route('/profile', name: 'app_profile_index')]
    public function index(Request $request, TranslatorInterface $translator): Response
    {
        if ($user = $this->getUser()) { 
            $translator->setlocale($user->getLang());

            $formUser = $this->createForm(ProfileFormType::class, $user, ['positionRightName' => $translator->trans('Right'),
                                                                          'positionLeftName' => $translator->trans('Left'),
                                                                          'positionHideName' => $translator->trans('Hide'),
                                                                         ]);
            $formUser->handleRequest($request);

            if ($formUser->isSubmitted() && $formUser->isValid())
            {
                $formUser = $formUser->getData();
                $this->entityManager->flush();

                return $this->redirectToRoute('app_book_index');
            }

            return $this->renderForm('profile/index.html.twig', ['email' => $user->getEmail(), 
                                                                 'form' => $formUser]);
        } else {
            throw $this->createAccessDeniedException("Wrong user");
        }
    }

    #[Route('/profile/get_panel_position', name: 'app_profile_panel')]
    public function getPanelPosition()
    {
        if ($user = $this->getUser()) {
            return new JsonResponse($user->getPanelPosition());
        } else {
            throw $this->createAccessDeniedException("Wrong user");
        }
    }
}