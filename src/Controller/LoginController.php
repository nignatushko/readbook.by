<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;

class LoginController extends AbstractController
{
    private $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    #[Route('/login', name: 'app_login')]
	public function index(AuthenticationUtils $authenticationUtils, TranslatorInterface $translator): Response
    {
        $auth_lang = $this->requestStack->getSession()->get('auth_lang');

        if ($auth_lang === 'ru' || $auth_lang === 'en') {
            $translator->setlocale($auth_lang);
        }

		$error = $authenticationUtils->getLastAuthenticationError();

		$lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('login/index.html.twig', [
			'last_username' => $lastUsername,
			'error'         => $error,
        ]);
    }

    #[Route('/login/changeLang/{lang}', name: 'app_login_change_lang')]
	public function changeLang(string $lang): Response
    {
        $this->requestStack->getSession()->set('auth_lang', $lang);

        return $this->redirectToRoute('app_login');
    }
}
