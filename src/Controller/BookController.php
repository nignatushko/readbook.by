<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use App\Entity\Book;
use App\Entity\WebBook;
use App\Entity\User;
use App\Form\BookTextFormType;
use App\Form\BookFileFormType;
use App\Form\BookUrlFormType;
use App\Form\BookLibruFormType;
use App\Form\BookProgressFormType;
use App\Form\BookSettingsFormType;
use App\Service\BookService;
use Doctrine\Persistence\ManagerRegistry;
use App\Service\TextService;
use App\ListClass;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\HeaderUtils;


class BookController extends AbstractController
{
    private $entityManager;
    private $webBookRepo;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->entityManager = $doctrine->getManager();
        $this->webBookRepo = $doctrine->getRepository(WebBook::class);
    }

    #[Route('/books/index', name: 'app_book_index')]
    public function index(BookService $bookService, TranslatorInterface $translator): Response
    {
        /**
         * @var User $user
        */
        $user = $this->getUser();    

        $translator->setlocale($user->getLang());

        $books = $user->getBooks();
        $output = [];
        foreach ($books as $book) {
            $progress = round($book->getCurrentPoint() / $bookService->getSpansCount($book->getFilePath())[0]['count'], 2) * 100;
            $output[] = ["id" => $book->getId(), "name" => $book->getName(), "progress" => $progress]; 
        }
        
        return $this->render('book/index.html.twig', ['books' => $output]);
    }

    #[Route('/books/show/{id}', name: 'app_book_show', requirements: ['id' => '\d+'])]
    public function show(Book $book, TextService $textService, BookService $bookService, TranslatorInterface $translator): Response
    {
        if ($book->getOwner() === $this->getUser()) {
            $translator->setlocale($this->getUser()->getLang());

            $currentPoint = $textService->getVars($this->getUser(),$book->getId())['currentPoint'];
            $spans = $bookService->getSpans($book->getFilePath(), $currentPoint - 100, $currentPoint + 100);
            $book->setLastDate(new \DateTime());
            $this->entityManager->flush();
            
            return $this->render('book/show.html.twig', ['book' => $book, 'spans' => $spans]);
        }
        else
        {
            throw $this->createAccessDeniedException("The book is not owned by the user");
        }

    }

    #[Route('/books/new/text', name: 'app_book_new_text')]
    public function newBookText(Request $request, BookService $bookService, TranslatorInterface $translator): Response
    {
        $translator->setlocale($this->getUser()->getLang());

        $book = new Book();
        $book->setCurrentPoint(1);
        $book->setLastDate(new \DateTime());

        $formText = $this->createForm(BookTextFormType::class, $book);
        $formText->handleRequest($request);

        if ($formText->isSubmitted() && $formText->isValid())
        {
            $book = $formText->getData();
            $text = $formText->get('Text')->getData();
            $book->setFilePath($bookService->createBook($text));
            $book->setOwner($this->getUser());
            switch ($bookService->getLanguageByContent($book->getFilePath())[0]['lang']) {
                case 'en':
                    $book->setVoice('slt');
                    break;
                case 'ru':
                    $book->setVoice('anna');
                    break;
            }

            $this->entityManager->persist($book);
            $this->entityManager->flush();
            return $this->redirectToRoute('app_book_index');
        }

        return $this->renderForm('book/new_text.html.twig', ['formText' => $formText]);  
    }

    #[Route('/books/new/file', name: 'app_book_new_file')]
    public function newBookFile(Request $request, BookService $bookService, TranslatorInterface $translator): Response
    {
        $translator->setlocale($this->getUser()->getLang());

        $book = new Book();
        $book->setCurrentPoint(1);
        $book->setLastDate(new \DateTime());

        $formFile = $this->createForm(BookFileFormType::class, $book);
        $formFile->handleRequest($request);

        if ($formFile->isSubmitted() && $formFile->isValid())
        {
            $book = $formFile->getData();
            $file = $formFile->get('File')->getData();
            $text = $bookService->getTextFromFile($file); 
            $book->setFilePath($bookService->createBook($text));
            $book->setOwner($this->getUser());
            switch ($bookService->getLanguageByContent($book->getFilePath())[0]['lang']) {
                case 'en':
                    $book->setVoice('slt');
                    break;
                case 'ru':
                    $book->setVoice('anna');
                    break;
            }

            $this->entityManager->persist($book);
            $this->entityManager->flush();
            return $this->redirectToRoute('app_book_index');
        }

        return $this->renderForm('book/new_file.html.twig', ['formFile' => $formFile]);  
    }

    #[Route('/books/new/url', name: 'app_book_new_url')]
    public function newBookUrl(Request $request, BookService $bookService, TranslatorInterface $translator): Response
    {
        $translator->setlocale($this->getUser()->getLang());

        $book = new Book();
        $book->setCurrentPoint(1);
        $book->setLastDate(new \DateTime());

        $formUrl = $this->createForm(BookUrlFormType::class, $book);
        $formUrl->handleRequest($request);

        if ($formUrl->isSubmitted() && $formUrl->isValid()) {
            $book = $formUrl->getData();
            $text = $bookService->getTextFromURL($formUrl->get('Url')->getData());
            $book->setFilePath($bookService->createBook($text));
            $book->setOwner($this->getUser());
            switch ($bookService->getLanguageByContent($book->getFilePath())[0]['lang']) {
                case 'en':
                    $book->setVoice('slt');
                    break;
                case 'ru':
                    $book->setVoice('anna');
                    break;
            }

            $this->entityManager->persist($book);
            $this->entityManager->flush();
            return $this->redirectToRoute('app_book_index');
        }

        return $this->renderForm('book/new_url.html.twig', ['formUrl' => $formUrl]);
    }

    #[Route('/books/new/web', name: 'app_book_new_web')]
    public function newBookWeb(Request $request, ManagerRegistry $managerRegistry, TranslatorInterface $translator, 
                               BookService $bookService): Response
    {
        $translator->setlocale($this->getUser()->getLang());

        $list = new ListClass('/books/new/web', $managerRegistry->getConnection("default"),
            'SELECT b.id,
                    b.book_name,
                    b.author_name,
                    b.book_url,
                    b.book_genre,
                    s.name
               FROM web_book b
                    INNER JOIN web_site s ON s.id = b.web_site_id
               WHERE 1 = 1
                    CHANGE_WHERE
              ORDER BY s.id, b.author_name, b.book_name
', $request);

        $list->addColumn('author_name', $translator->trans('Author name'));
        $list->addColumn('book_name', $translator->trans('Book name'), 
                         "SELECT ts_headline('CHANGE_LANG', 'CHANGE_TARGET', 
                         plainto_tsquery('CHANGE_LANG', 'CHANGE_QUERY'), 'HighlightAll=TRUE')", 'ts_headline');
        $list->addColumn('book_genre', $translator->trans('Book genre'));
        $list->addColumn('name', $translator->trans('By libru'));

        $list->addSearch('author_name', 'author_name', $request->query->get('author_name') ? $request->query->get('author_name') : '', false);
        $list->addSearch('book_name', 'book_name', $request->query->get('book_name') ? $request->query->get('book_name') : '', true);
        $list->addSearch('book_genre', 'book_genre', $request->query->get('book_genre') ? $request->query->get('book_genre') : '', false);
        $list->addSearch('name', 'name', $request->query->get('name') ? $request->query->get('name') : '', false, $bookService->getWebSites($this->entityManager));

        $list->setSearchName($translator->trans('Search'));
        
        $list->setAction('/books/create/web', $translator->trans('Save'), ['id']);
        $list->setActionColName($translator->trans('Action'));
        $htmlArr = $list->getHTML();

        return $this->render('book/new_web.html.twig', ['htmlList' => $htmlArr['htmlList'], 
                                                        'htmlPages' => $htmlArr['htmlPages'],
                                                        'htmlSearch' => $htmlArr['htmlSearch'],
                                                        'rowsCount' => $htmlArr['rowsCount']]);
    }

    #[Route('/books/create/web', name: 'app_book_create_web')]
    public function createBookWeb(Request $request, BookService $bookService, TranslatorInterface $translator): Response
    {
        $translator->setlocale($this->getUser()->getLang());

        $webbook = $this->entityManager->find('App\Entity\WebBook', $request->query->get('id'));
        $book = new Book();
        $book->setCurrentPoint(1);
        $book->setLastDate(new \DateTime());
        $book->setName($webbook->getBookName() . ' - ' . $webbook->getAuthorName());
        $text = $bookService->getTextFromURL($webbook->getBookUrl());
        $book->setFilePath($bookService->createBook($text));
        $book->setOwner($this->getUser());
        switch ($bookService->getLanguageByContent($book->getFilePath())[0]['lang']) {
            case 'en':
                $book->setVoice('slt');
                break;
            case 'ru':
                $book->setVoice('anna');
                break;
        }
        $this->entityManager->persist($book);
        $this->entityManager->flush();
        return $this->redirectToRoute('app_book_index');
    }

    #[Route('/books/delete/{id}', name: 'app_book_delete', requirements: ['id' => '\d+'])]
    public function delete(Book $book, BookService $bookService): Response
    {
        if ($book->getOwner() === $this->getUser()) {
            $bookService->deleteBook($book->getFilePath());
            $this->getUser()->removeBook($book);
            $this->entityManager->remove($book);
            $this->entityManager->flush();
        }
        else
        {
            throw $this->createAccessDeniedException("The book is not owned by the user");
        }
        
        return $this->redirectToRoute('app_book_index');
    }

    #[Route('/books/edit/{id}', name: 'app_book_edit', requirements: ['id' => '\d+'])]
    public function update(Request $request, Book $book, BookService $bookService, TranslatorInterface $translator): Response
    {
        if ($book->getOwner() === $this->getUser()) {
            $translator->setlocale($this->getUser()->getLang());

            $formText = $this->createForm(BookTextFormType::class, $book);
            $formText->handleRequest($request);
            $bookText = $bookService->getPlainText($book->getFilePath());

            if ($formText->isSubmitted() && $formText->isValid())
            {
                $book = $formText->getData();
                $text = $formText->get('Text')->getData();
                $bookService->deleteBook($book->getFilePath());
                $bookService->createBook($text, true, $book->getFilePath());
                $book->setLastDate(new \DateTime());
                $book->setCurrentPoint(1);
                $this->entityManager->flush();

                return $this->redirectToRoute('app_book_show', array('id' => $book->getId()));
            }

            return $this->renderForm('book/edit.html.twig', ['formText' => $formText, 'bookText' => $bookText, 'book' => $book]);
        }
        else {
            throw $this->createAccessDeniedException("The book is not owned by the user");
        }
    }

    #[Route('/books/settings/{id}', name: 'app_book_settings', requirements: ['id' => '\d+'])]
    public function settings(Request $request, Book $book, BookService $bookService, TranslatorInterface $translator): Response
    {
        if ($book->getOwner() === $this->getUser()) {
            $translator->setlocale($this->getUser()->getLang());

            $formSettings = $this->createForm(BookSettingsFormType::class, $book, ['currentVoice' => $book->getVoice(),
                'currentSpeed' => $book->getSpeed()]);
            $formSettings->handleRequest($request);

            if ($formSettings->isSubmitted() && $formSettings->isValid()) {
                $book = $formSettings->getData();

                $this->entityManager->flush();
                return $this->redirectToRoute('app_book_show', array('id' => $book->getId()));
            }

            return $this->renderForm('book/settings.html.twig', ['formSettings' => $formSettings, 'book' => $book]);
        }
        else {
            throw $this->createAccessDeniedException("The book is not owned by the user");
        }

    }

    #[Route('/books/smile', name: 'app_book_smile')]
    public function smile(BookService $bookService, TranslatorInterface $translator): Response
    {
		$webbook = $this->entityManager->find('App\Entity\WebBook', 456091);
		return new Response(
					$webbook->getBookUrl()
				);
    }


    #[Route('/books/progress/{id}', name: 'app_book_progress', requirements: ['id' => '\d+'])]
    public function progress(Request $request, Book $book, BookService $bookService, TranslatorInterface $translator): Response
    {
        if ($book->getOwner() === $this->getUser()) {
            $translator->setlocale($this->getUser()->getLang());

            $formProgress = $this->createForm(BookProgressFormType::class, $book, ['currentProgress' => $book->getCurrentPoint(),
                'maxProgress' => $bookService->getSpansCount($book->getFilePath())[0]['count']]);
            $formProgress->handleRequest($request);

            if ($formProgress->isSubmitted() && $formProgress->isValid()) {
                $book->setCurrentPoint($formProgress->get('progress')->getData());
                $this->entityManager->flush();
                return $this->redirectToRoute('app_book_show', array('id' => $book->getId()));
            }

            return $this->renderForm('book/progress.html.twig', ['formProgress' => $formProgress, 'book' => $book]);
        }
        else {
            throw $this->createAccessDeniedException("The book is not owned by the user");
        }

    }

    #[Route('/books/search_span/{id}', name: 'app_book_search_span', requirements: ['id' => '\d+'])]
    public function searchSpan(Request $request, Book $book, ManagerRegistry $managerRegistry, BookService $bookService,
                               TranslatorInterface $translator)
    {
        if ($book->getOwner() === $this->getUser()) {   
            $translator->setlocale($this->getUser()->getLang());
    
            $list = new ListClass('/books/search_span/' . $book->getId(), $managerRegistry->getConnection("readbooks"), "
                SELECT b.id,
                       b.span,
                       round(100::numeric * b.id::numeric / m.id::numeric, 2) as perc
                  FROM " . $book->getFilePath() . " AS b
                       INNER JOIN (SELECT id FROM " . $book->getFilePath() . " ORDER BY 1 DESC LIMIT 1) as m ON 1 = 1
                 WHERE 1 = 1
                CHANGE_WHERE
                 ORDER BY b.id
            ", $request);

            $list->addColumn('span', $translator->trans('Book part'), 
                             "SELECT ts_headline('CHANGE_LANG', 
                                                 'CHANGE_TARGET', 
                                                 plainto_tsquery('CHANGE_LANG', 'CHANGE_QUERY'), 'HighlightAll=TRUE')",
                            'ts_headline');
            $list->addColumn('perc', $translator->trans('Progress, %'));

            $list->addSearch('span', 'span', $request->query->get('span') ? $request->query->get('span') : '', true);
            $list->setSearchName($translator->trans('Search'));
            
            $list->setAction('/books/go_to_span/' . $book->getId(), $translator->trans('Search'), ['id']);
            $list->setActionColName($translator->trans('Action'));

            $htmlArr = $list->getHTML();

            return $this->render('book/search.html.twig', ['htmlList' => $htmlArr['htmlList'],      
                                                           'htmlPages' => $htmlArr['htmlPages'],
                                                           'htmlSearch' => $htmlArr['htmlSearch'],
                                                           'rowsCount' => $htmlArr['rowsCount'],
                                                           'book' => $book]);
        } else {
            throw $this->createAccessDeniedException("The book is not owned by the user");
        }
    }

    #[Route('/books/history/{id}', name: 'app_book_history', requirements: ['id' => '\d+'])]
    public function bookHistory(Request $request, Book $book, ManagerRegistry $managerRegistry, BookService $bookService,
                               TranslatorInterface $translator)
    {
        if ($book->getOwner() === $this->getUser()) {   
            $translator->setlocale($this->getUser()->getLang());
    
            $list = new ListClass('/books/history/' . $book->getId(), $managerRegistry->getConnection("readbooks"), "
                SELECT b.id,
                       b.span,
                       round(100::numeric * b.id::numeric / m.id::numeric, 2) as perc,
                       to_char(b.lastupdate, 'yyyy-mm-dd hh24:mi:ss') as span_lastupdate
                  FROM " . $book->getFilePath() . " AS b
                       INNER JOIN (SELECT id FROM " . $book->getFilePath() . " ORDER BY 1 DESC LIMIT 1) as m ON 1 = 1
                 WHERE b.lastupdate IS NOT NULL
                    CHANGE_WHERE
                 ORDER BY b.lastupdate DESC", $request);

            $list->addColumn('span', $translator->trans('Book part'), 
                             "SELECT ts_headline('CHANGE_LANG', 
                                                 'CHANGE_TARGET', 
                                                 plainto_tsquery('CHANGE_LANG', 'CHANGE_QUERY'), 'HighlightAll=TRUE')",
                            'ts_headline');
            $list->addColumn('perc', $translator->trans('Progress, %'));

            $list->addColumn('span_lastupdate', $translator->trans('Timestamp'));

            $list->addSearch('span', 'span', $request->query->get('span') ? $request->query->get('span') : '', true);
            $list->addSearch('span_lastupdate', "to_char(lastupdate, 'yyyy-mm-dd hh:mi:ss')", $request->query->get('span_lastupdate') ? $request->query->get('span_lastupdate') : '', false);
            $list->setSearchName($translator->trans('Search'));
            
            $list->setAction('/books/go_to_span/' . $book->getId(), $translator->trans('Go'), ['id']);
            $list->setActionColName($translator->trans('Go'));

            $htmlArr = $list->getHTML();

            return $this->render('book/history.html.twig', ['htmlList' => $htmlArr['htmlList'],      
                                                           'htmlPages' => $htmlArr['htmlPages'],
                                                           'htmlSearch' => $htmlArr['htmlSearch'],
                                                           'rowsCount' => $htmlArr['rowsCount'],
                                                           'book' => $book]);
        } else {
            throw $this->createAccessDeniedException("The book is not owned by the user");
        }
    }

    #[Route('/books/go_to_span/{id}', name: 'app_book_go_to_span', requirements: ['id' => '\d+'])]
    public function goToSpan(Request $request, Book $book, TextService $textService)
    {
        if ($book->getOwner() === $this->getUser()) {
            $goToPoint = $request->query->get('id');
            $textService->saveVars($this->getUser(), $book->getId(), $goToPoint);
            return $this->redirectToRoute('app_book_show', array('id' => $book->getId()));
        } else {
            throw $this->createAccessDeniedException("The book is not owned by the user");
        }
    }

    #[Route('/books/statistics/{id}', name: 'app_book_statistics', requirements: ['id' => '\d+'])]
    public function statistics(Book $book, BookService $bookService, TranslatorInterface $translator)
    {
        if ($book->getOwner() === $this->getUser()) {
            $translator->setlocale($this->getUser()->getLang());

            $currentPoint = $book->getCurrentPoint();
            $tableName = $book->getFilePath();
            $lastPoint = $bookService->getSpansCount($tableName)[0]['count'];
            $percentageProgress = round(100 * $currentPoint / $lastPoint, 2);
            
            return $this->render('book/statistics.html.twig', ['name' => $book->getName(),
                                                               'id' => $book->getId(), 
                                                               'currentPoint' => $currentPoint,
                                                               'lastPoint' => $lastPoint,
                                                               'percentageProgress' => $percentageProgress,
                                                               'allWordsCount' => $bookService->getWordsCount($tableName),
                                                               'currentWordsCount' => $bookService
                                                                                      ->getWordsCount($tableName, $currentPoint),
                                                               'allSize' => $bookService->getSpanByteSize($tableName),
                                                               'wordsByDate' => $bookService->getWordsByDate($tableName),
                                                               'currentSize' => $bookService
                                                                            ->getSpanByteSize($tableName, $currentPoint)]);
        } else {
            throw $this->createAccessDeniedException("The book is not owned by the user");
        }
    }

    #[Route('/books/download/{id}', name: 'app_book_download', requirements: ['id' => '\d+'])]
    public function download(Book $book, BookService $bookService,)
    {
        if ($book->getOwner() === $this->getUser()) {
            $fileContent = $bookService->getPlainText($book->getFilePath());
            $response = new Response($fileContent);

            $disposition = HeaderUtils::makeDisposition(
                HeaderUtils::DISPOSITION_ATTACHMENT,
                preg_replace('/[^\w$\x{0080}-\x{FFFF}]+/u', '_', $book->getName()) . '.txt',
                'book.txt'
            );
            $response->headers->set('Content-Disposition', $disposition);

            return $response;
        } else {
            throw $this->createAccessDeniedException("The book is not owned by the user");
        }
    }
    
    #[Route('/books/test', name: 'app_book_test')]
    public function test(BookService $bookService)
    {
        if ($user = $this->getUser()) {
            return $user->getPanelPosition();
        } else {
            throw $this->createAccessDeniedException("Wrong user");
        }
    }

    #[Route('/books/muller/{id}', name: 'app_book_muller', requirements: ['id' => '\d+'])]
    public function searchMuller(Request $request, Book $book, ManagerRegistry $managerRegistry, BookService $bookService,
                               TranslatorInterface $translator)
    {
        if ($book->getOwner() === $this->getUser()) {   
            $translator->setlocale($this->getUser()->getLang());
    
        $list = new ListClass('/books/muller/' . $book->getId(), $managerRegistry->getConnection("default"),
            "
                 SELECT id, REGEXP_REPLACE(word, e'\\n', e'<br/>', 'g') as word
                   FROM muller
                  WHERE 1 = 1
                        CHANGE_WHERE
                  ORDER BY id
            ", $request);

        $list->addColumn('word', $translator->trans('Translate'), 
                         "SELECT ts_headline('CHANGE_LANG', 'CHANGE_TARGET', 
                         plainto_tsquery('CHANGE_LANG', 'CHANGE_QUERY'), 'HighlightAll=TRUE')", 'ts_headline');

        $list->addSearch('word', 'word', $request->query->get('word'), true);
        $list->setSearchName($translator->trans('Search'));
            
            $htmlArr = $list->getHTML();

            return $this->render('book/muller.html.twig', ['htmlList' => $htmlArr['htmlList'],      
                                                           'htmlPages' => $htmlArr['htmlPages'],
                                                           'htmlSearch' => $htmlArr['htmlSearch'],
                                                           'rowsCount' => $htmlArr['rowsCount'],
                                                           'book' => $book]);
        } else {
            throw $this->createAccessDeniedException("The book is not owned by the user");
        }
    }

}
