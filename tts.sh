#!/bin/sh

RATE="0"
VOICE="Anna"
while getopts ":r:s:" OPT
do
	case $OPT in
		r ) RATE="$OPTARG" ;;
		s ) VOICE="$OPTARG" ;;
	esac
done
shift $(($OPTIND - 1))

fileDBUS="/var/www/readbook.by/DBUS"
if [ -f "$fileDBUS" -a -n "$(pgrep RHVoice-service)" ]
then
	export $(cat "$fileDBUS")
else
	eval dbus-launch
	export $(dbus-launch | tee "$fileDBUS")
fi
cat /dev/stdin | RHVoice-client -v 1 -s "$VOICE+slt" -r "$RATE" | lame -b 64 - 
