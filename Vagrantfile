# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
# $default_network_interface = `ip route | grep -E "^default" | awk '{printf "%s", $5; exit 0}'`
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "ubuntu/jammy64"
  #config.vbguest.auto_update = false

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  #config.vm.network "public_network", bridge: ["#$default_network_interface", "en0: Wi-Fi (AirPort)"]
  #config.vm.network "private_network", ip: "192.168.56.10"
  config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "0.0.0.0"
  config.vm.network "forwarded_port", guest: 5432, host: 5432, host_ip: "0.0.0.0"
  config.vm.network "forwarded_port", guest: 5433, host: 5433, host_ip: "0.0.0.0"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  #config.vm.synced_folder ".", "/var/www/readbook.by", owner: "www-data", group: "www-data", type: "rsync", rsync__args: ["--verbose", "--archive", "--delete", "-z"]
  config.vm.synced_folder ".", "/var/www/readbook.by", owner: "www-data", group: "www-data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    vb.customize ["modifyvm", :id, "--vram", "128"]
    vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/readbook.by", "1"]
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
  
  config.vm.provision "shell", run: "once", inline: <<-SHELL
    sudo fallocate -l 2G /swapfile
    sudo chmod 600 /swapfile
    sudo mkswap /swapfile
    sudo swapon /swapfile
    sudo echo "/swapfile                       none            swap    sw              0       0" >> /etc/fstab
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
    sed -i 's/archive.ubuntu.com/by.archive.ubuntu.com/' /etc/apt/sources.list
    apt update;
    apt install -y \
      antiword \
      apache2 \
      ca-certificates \
      enca \
      dbus-x11 \
      lame \
      libapache2-mod-php8.1 \
      pandoc \
      pgbouncer \
      php \
      php-xml \
      php-xml \
      php-mbstring \
      php-intl \
      php-pdo-pgsql \
      php-curl \
      php-zip \
      php-redis \
      php-gd \
      php-amqp \
      poppler-utils \
      postgresql \
      rhvoice \
      rhvoice-english \
      rhvoice-russian \
      speech-dispatcher-rhvoice \
      w3m \
      yarn;
    fallocate -l 1G /swapfile;
    chmod 600 /swapfile;
    mkswap /swapfile;
    swapon /swapfile;
    a2enmod ssl;
    a2enmod rewrite;
    mkdir /etc/apache2/ssl 
    openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=readbook.by" -keyout /etc/apache2/ssl/apache.key -out /etc/apache2/ssl/apache.cert;
    cat /etc/apache2/ssl/apache.key /etc/apache2/ssl/apache.cert > /etc/apache2/ssl/apache.pem;

    echo "
      <VirtualHost *:443>
        ServerAdmin admin@readbook.by
        ServerName readbook.by
        SSLEngine On
        SSLCertificateFile /etc/apache2/ssl/apache.pem
        DocumentRoot /var/www/readbook.by/public
        <Directory /var/www/readbook.by/public>
            AllowOverride All
            Order Allow,Deny
            Allow from All
        </Directory>
        ErrorLog ${APACHE_LOG_DIR}/bukaby_error.log
        CustomLog ${APACHE_LOG_DIR}/bukaby_access.log combined
      </VirtualHost> 
    " >> /etc/apache2/apache2.conf;

    echo "
      <VirtualHost *:80>
        ServerAdmin admin@readbook.by
        DocumentRoot /var/www/readbook.by/public
        <Directory /var/www/readbook.by/public>
            AllowOverride All
            Order Allow,Deny
            Allow from All
        </Directory>
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
      </VirtualHost>
    " > /etc/apache2/sites-enabled/000-default.conf

    sed -i '/^post_max_size/s/^.*$/post_max_size = 0/' /etc/php/8.1/apache2/php.ini
    sed -i '/^upload_max_filesize/s/^.*$/upload_max_filesize = 0/' /etc/php/8.1/apache2/php.ini
    sed -i '/^max_execution_time/s/^.*$/max_execution_time = 0/' /etc/php/8.1/apache2/php.ini
    sed -i '/^memory_limit/s/^.*$/memory_limit = -1/' /etc/php/8.1/apache2/php.ini

    systemctl restart apache2;
    # php composer
    cd /usr/bin
    wget https://getcomposer.org/installer
    php installer
    chmod +x composer.phar
    mv composer.phar composer
    # symfony cli
    echo 'deb [trusted=yes] https://repo.symfony.com/apt/ /' | sudo tee /etc/apt/sources.list.d/symfony-cli.list
    apt update
    apt install symfony-cli 
    cd /var/www/readbook.by;
    COMPOSER_ALLOW_SUPERUSER=1 composer install;
    mkdir -p /var/www/readbook.by/var/{logs,cache};
    chmod 0777 -R /var/www/readbook.by/var;
    # postgres
    sed -i 's/peer/trust/;'  /etc/postgresql/14/main/pg_hba.conf;
    sed -i 's/md5/trust/;'  /etc/postgresql/14/main/pg_hba.conf;
    sed -i 's/scram-sha-256/trust/;'  /etc/postgresql/14/main/pg_hba.conf;
    echo "host    all             all             0.0.0.0/0           trust" >> /etc/postgresql/14/main/pg_hba.conf;
    sed -i '/listen_addresses/{s/#//; s/localhost/*/;};' /etc/postgresql/14/main/postgresql.conf;
    sed -i '/#log_statement /{s/#//; s/none/all/;};' /etc/postgresql/14/main/postgresql.conf;
    sed -i '/^port = /{s/5432/5433/;};' /etc/postgresql/14/main/postgresql.conf;
    sed -i '/listen_port/{s/6432/5432/};' /etc/pgbouncer/pgbouncer.ini;
    sed -i '/;bardb/{s/;bardb.*/* = port=5433 auth_user=postgres/};' /etc/pgbouncer/pgbouncer.ini;
    sed -i '/^;admin_users/{s/^;admin_users.*/admin_users = postgres/};' /etc/pgbouncer/pgbouncer.ini;
    sed -i '/^;stats_users/{s/^;stats_users.*/stats_users = postgres/};' /etc/pgbouncer/pgbouncer.ini;
    sed -i '/^;max_client_conn/{s/100/1000/; s/;//;};' /etc/pgbouncer/pgbouncer.ini;
    sed -i '/^;default_pool_size/{s/20/300/; s/;//;};' /etc/pgbouncer/pgbouncer.ini;
    systemctl restart postgresql;
    systemctl restart pgbouncer;
    # install js libs
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
    eval "$(grep -vFx '[ -z "$PS1" ] && return' /root/.bashrc)"
    nvm install v20
    nvm use v20
    nvm alias default node
    cd /var/www/readbook.by
    yarn install
    yarn encore dev
    #migrate tables
    php bin/console doctrine:database:create
    php bin/console doctrine:migrations:migrate <<< yes
    cat /var/www/readbook.by/provision/*.sql | psql -U postgres;
    ip addr
    #delete old books
    rm book_storage/* -rf
    #create database for books
    psql -U postgres -c "create database readbooks";
  SHELL

  config.trigger.after :up, :reload do |trigger|
    trigger.info = "Clear temp stuff"
    trigger.run_remote = {
      inline: "\
        rm -rf /var/www/buka.by/var/logs/* /var/www/buka.by/var/cache/*;\
      "
    }
  end
end

