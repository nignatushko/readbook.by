<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221011090734 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("
            CREATE TABLE public.web_site (
                id SERIAL NOT NULL,
                name character varying(100) NOT NULL
            );
        ");
        $this->addSql("
            ALTER TABLE ONLY public.web_site
                ADD CONSTRAINT web_site_pkey PRIMARY KEY (id);
        ");
        $this->addSql("
            CREATE TABLE public.web_book (
                id SERIAL NOT NULL,
                web_site_id integer,
                book_name character varying(2000) NOT NULL,
                book_url character varying(2000) NOT NULL,
                author_name character varying(3000) NOT NULL,
                book_genre character varying(2000) NOT NULL
            );
        ");
        $this->addSql("
            ALTER TABLE ONLY public.web_book
                ADD CONSTRAINT web_book_pkey PRIMARY KEY (id);
        ");
        $this->addSql("
            CREATE INDEX web_book_book_name_idx ON public.web_book USING btree (web_site_id);
        ");
        $this->addSql("
            CREATE INDEX web_book_author_name_idx ON public.web_book USING btree (web_site_id);
        ");
        $this->addSql("
            ALTER TABLE ONLY public.web_book
                ADD CONSTRAINT web_book_web_site_id_fkey FOREIGN KEY (web_site_id) REFERENCES public.web_site(id);
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE web_book');
        $this->addSql('DROP TABLE web_site');
    }
}
