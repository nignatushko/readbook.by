#!/bin/bash

E_NOARGS=86

if [ -z "$1" ]
then
	echo "Usage $0 BOOK_ID"
	exit $E_NOARGS 
fi

BOOK_ID="$1"
# starting url
URL="http://www.mirrorservice.org/sites/ftp.ibiblio.org/pub/docs/books/gutenberg"

# add BOOK_ID numbers but last to URL 
for (( i=0; i<$((${#BOOK_ID}-1)); i++ )); do
	URL="$URL/${BOOK_ID:$i:1}"
done

# add whole BOOK_ID to end 
URL="$URL/${BOOK_ID}/"

# find txt file
URL="$URL/"$(curl --silent "$URL" | grep -m 1 -o 'href="[^"]\+.txt"' | sed 's/href=//; s/"//g;')

echo $URL
