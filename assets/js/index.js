const showOptions = document.getElementById('show_options');
const newOptions = document.getElementById('add-options');

const personalOptions = document.getElementById('personal-options');
const showPersonalOptions = document.getElementById('show-personal-options');

showOptions.onclick = function ()
{
    newOptions.classList.toggle('hide');
    showOptions.classList.toggle('black-back');
    personalOptions.classList.add('hide');
    showPersonalOptions.classList.remove('black-back');
}

newOptions.onmouseover = function ()
{
    newOptions.classList.remove('hide');
    showOptions.classList.add('black-back');
}

newOptions.onmouseout = function ()
{
    newOptions.classList.add('hide');
    showOptions.classList.remove('black-back');
}

showPersonalOptions.onclick = function ()
{
    personalOptions.classList.toggle('hide');
    showPersonalOptions.classList.toggle('black-back');
    newOptions.classList.add('hide');
    showOptions.classList.remove('black-back');
}

window.onfocus = function () 
{
    console.log('focus');
}

window.onblur = function () 
{
    console.log('blur');
}