const bookId = parseInt(document.getElementById("book-id").getAttribute("info"));

const setWordsText = document.getElementById("set-book-words");
const setWordsButton = document.getElementById("set-book-words-btn");
const searchedList = document.getElementById("searched-list");
const searchedPages = document.getElementById("searched-pages");

setWordsButton.onclick = function () {
    search(1);
};

function search(targetPage) {
    const xhttpGetSpans = new XMLHttpRequest();
    xhttpGetSpans.open("GET", "/books/search_span/" + bookId + "?query=" + setWordsText.value + "&page=" + targetPage);
    xhttpGetSpans.send();

    xhttpGetSpans.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            const vars = JSON.parse(this.response);
            let searches = vars.searches;
            let pagePad = vars.pagePad;
            
            searchedList.innerHTML = "";
            searches.forEach(span => {
                temp = "";
                temp += "<tr>";
                temp += "<td>" + span.ts_headline + "</td>";
                temp += "<td><button id='search_" + span.id + "' class='btn' type='button'>GO</button></td>";
                temp += "</tr>";
                searchedList.innerHTML += temp;
                
            });
            searches.forEach(span => {
                document.getElementById("search_" + span.id).onclick = async function () {
                    window.location.assign('/books/go_to_span/' + bookId + '?getToPoint=' + span.id);
                };
            });

			pagePadKeys = Object.keys(pagePad);
            sortedPagePadKeyStart = [];
            if (pagePad["<<"]) {
                sortedPagePadKeyStart.push(pagePadKeys.splice(pagePadKeys.indexOf("<<"), 1)[0]);
            }
            if (pagePad["<"]) {
                sortedPagePadKeyStart.push(pagePadKeys.splice(pagePadKeys.indexOf("<"), 1)[0]);
            }

            sortedPagePadKeyEnd = [];
            if (pagePad[">"]) {
                sortedPagePadKeyEnd.push(pagePadKeys.splice(pagePadKeys.indexOf(">"), 1)[0]);
            }
            if (pagePad[">>"]) {
                sortedPagePadKeyEnd.push( pagePadKeys.splice(pagePadKeys.indexOf(">>"), 1)[0]);
            }

            sortedPagePadKeys = sortedPagePadKeyStart.concat(pagePadKeys, sortedPagePadKeyEnd);
            searchedPages.innerHTML = "";
            sortedPagePadKeys.forEach(key => {
                idPage = 'page_' + key;
                searchedPages.innerHTML += '<div id=\'' + idPage +'\'>' + key +'</div>';
            });
            sortedPagePadKeys.forEach(key => {
                idPage = 'page_' + key;
                document.getElementById(idPage).onclick = function () {
                    search(pagePad[key]);
                };
            });
        }
    }
}
