let currentPoint = parseInt(document.getElementById("current-point").getAttribute("info"));
let spans;
let spanCount;
const bookId = parseInt(document.getElementById("book-id").getAttribute("info"));

const spanCollection = document.getElementById("span-collection");
const readDiv = document.getElementById("read");
const navContainerDiv = document.getElementById("nav-container");
const bookOptions = document.getElementById("book-options");
const personalOptions = document.getElementById("personal-options");
const nonActiveBackDiv = document.getElementById("non-active-back");
const deleteForm = document.getElementById("delete-form");
const navPlay = document.getElementById("nav-play");
const navTranslate = document.getElementById("nav-translate");

const playButton = document.getElementById("play");
const nextButton = document.getElementById("next");
const backButton = document.getElementById("back");
const searchButton = document.getElementById("show-search-panel");
const translateButton = document.getElementById("translate");
const selectButton = document.getElementById("select");
const deleteButton = document.getElementById("delete");
const showBookOptions = document.getElementById("show-book-options");
const showPersonalOptions = document.getElementById("show-personal-options");
const deleteYesButton = document.getElementById("yes-btn");
const deleteNoButton = document.getElementById("no-btn");

const progBar = document.getElementById("progbar").children[0];

const setWordsText = document.getElementById("set-book-words");
const setWordsButton = document.getElementById("set-book-words-btn");
const searchedList = document.getElementById("searched-list");

let currentVoice = 'anna';
let currentSpeed = 0.1;

const playlist = new Map();

const range = document.createRange();
const selection = window.getSelection();

let bookEnd = false;
let bookStart = false;

const xhttpgetPanelPosition = new XMLHttpRequest();

xhttpgetPanelPosition.open("GET", "/profile/get_panel_position");
xhttpgetPanelPosition.send();

resetSpans();  

xhttpgetPanelPosition.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
        const position = JSON.parse(this.response);
        if (position !== null && position !== 'Hide') {
            navContainerDiv.classList.remove('hide');
            if (position === 'Right') {
                navContainerDiv.classList.add('fix-right');
            }
        }
    }
};    

showBookOptions.onclick = function ()
{
    bookOptions.classList.toggle('hide');
    showBookOptions.classList.toggle('black-back');
    personalOptions.classList.add('hide');
    showPersonalOptions.classList.remove('black-back');
}

bookOptions.onmouseover = function ()
{
    bookOptions.classList.remove('hide');
    showBookOptions.classList.add('black-back');
}

bookOptions.onmouseout = function ()
{
    bookOptions.classList.add('hide');
    showBookOptions.classList.remove('black-back');
}

showPersonalOptions.onclick = function ()
{
    personalOptions.classList.toggle('hide');
    showPersonalOptions.classList.toggle('black-back');
    bookOptions.classList.add('hide');
    showBookOptions.classList.remove('black-back');
}

deleteButton.onclick = function () {
    nonActiveBackDiv.classList.remove("hide");
    deleteForm.classList.remove("hide");
}

deleteNoButton.onclick = function () {
    nonActiveBackDiv.classList.add("hide");
    deleteForm.classList.add("hide");
}

deleteYesButton.onclick = function () {
    window.location.href = "/books/delete/" + bookId;
}

playButton.onclick = playOrPauseAction;

navPlay.onclick = function () {
    playOrPauseAction();
    bookOptions.classList.add('hide');
    showBookOptions.classList.remove('black-back');
}

nextButton.onclick = nextAction;

backButton.onclick = backAction;

translateButton.onclick = translateAction;

navTranslate.onclick = function () {
    translateAction();
    bookOptions.classList.add('hide');
    showBookOptions.classList.remove('black-back');
}

window.onkeydown = setHotKeys;

window.onload = getVarsFromDB;

window.onblur = function () {
    if (!playlist.get(currentPoint).paused) {
        playButton.innerHTML = "<i class='fa-solid fa-play'></i>";
        playlist.get(currentPoint).pause();
    }
};

selectButton.onclick = getVarsFromDB;

readDiv.onscroll = addMoreSpans;

function escapeRegExp(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

function fuzzyMatch(pattern, str) {
  pattern = '.*' + pattern.split('').map(l => `${escapeRegExp(l)}.*`).join('');
  const re = new RegExp(pattern);
  return re.test(str);
}

async function addMoreSpans () {
    if (readDiv.scrollTop === 0) {
        await getAdditionalUpSpansPromises(50);
    }
    if (readDiv.offsetHeight + readDiv.scrollTop + 5 >= readDiv.scrollHeight) {
        await getAdditionalDownSpansPromises(50);
    }
};

function getVarsFromDB () {
    const xhttpSetVars = new XMLHttpRequest();

    xhttpSetVars.open("GET", "/text/get_vars?book_id=" + bookId);
    xhttpSetVars.send();
    
    xhttpSetVars.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            const vars = JSON.parse(this.response);
            if (vars.voice !== null)
                currentVoice = vars.voice;
            if (vars.speed !== null)
                currentSpeed = (vars.speed / 10);
            if (vars.spanCount !== null)
                spanCount = vars.spanCount[0]['count'];  
            if (vars.currentPoint !== null)
                currentPoint = vars.currentPoint;
            if (!getSpan(currentPoint))
                location.reload();
            if (!isElementFullyVisible(getSpan(currentPoint))) {
                getSpan(currentPoint).scrollIntoView({behavior: "auto", block: "start"});
            }
            selectSpan(currentPoint);          
        }
    };
};

async function playOrPauseAction () {
    createAudioIfNotExist(currentPoint);
    selectSpan(currentPoint);
    if (playlist.get(currentPoint).paused) {
        if ('wakeLock' in navigator) {
            wakeLock = await navigator.wakeLock.request("screen");
        }

        playButton.innerHTML = "<i class='fa-solid fa-pause'></i>";
        //navPlay.innerHTML = "<i class='fa-solid fa-pause'></i>";
        await play();
    }
    else {
        if ('wakeLock' in navigator) {
            wakeLock.release();
        }
        
        playButton.innerHTML = "<i class='fa-solid fa-play'></i>";
        //navPlay.innerHTML = "<i class='fa-solid fa-play'></i>";
        playlist.get(currentPoint).pause();
    }
}

async function translateAction () {
    const translatePoint = currentPoint;
    const closeSpanId = "s" + translatePoint + "-close";
    if (!document.getElementById(closeSpanId)) {
        if (playlist.get(currentPoint) && !playlist.get(currentPoint).paused) {
            playlist.get(currentPoint).pause();
        }
        const xhttpGetTranslation = new XMLHttpRequest();
        const text = getSpan(translatePoint).textContent;
        xhttpGetTranslation.open("GET", "/text/get_translate?text=" + encodeURIComponent(text));
        xhttpGetTranslation.send();

        xhttpGetTranslation.onreadystatechange = async function () {
            if (this.readyState === 4 && this.status === 200) {
                let translateText = "";
                const translateArray = JSON.parse(this.response);
                translateText += "<textarea id=\"prompt" + translatePoint + "\"></textarea>" +
                            "<div id='" + closeSpanId + "' class='container-centered close-span'>" +
                            "<div><audio id='speaker_llm" + translatePoint + "' preload='auto'></audio></div>" + 
                            "<div><button class='button button4' title='Read Aloud' id='button_llm_speaker" + translatePoint + "'><i class='fa-solid fa-play'></i></button></div>" +
                            "<div><button class='button button4' title='Explain by Parts' id='button_llm_explain" + translatePoint + "');'><i class='fa-solid fa-person-digging'></i></button></div>" +
                            "<div><button class='button button4' title='Simplify' id='button_llm_rephrase" + translatePoint + "');'><i class='fa-solid fa-quote-right'></i></button></div>" +
                            "<div><button class='button button4' title='Idioms' id='button_llm_idioms" + translatePoint + "');'><i class='fa-solid fa-i'></i></button></div>" +
                            "<div><button class='button button4' title='Verbs' id='button_llm_verbs" + translatePoint + "');'><i class='fa-solid fa-v'></i></button></div>" +
                            "<div><button class='button button4' title='Send' id='button_llm_any" + translatePoint + "');'><i class='fa-solid fa-right-to-bracket'></i></button></div>" +
                            "</div>";
                for (let i in translateArray) {
                    const spanOpenId = "s" + translatePoint + "-o" + i;
                    const spanTranslateId = "s" + translatePoint + "-t" + i;

                    linkCurrentBook = "<div><button class='button button4' onclick='event.stopPropagation(); window.location.href = \"/books/search_span/" + bookId 
                                                                             + "?span=" 
                                                                             + translateArray[i].name 
                                                                             + "\"'><i class='fa-solid fa-search'></i></button></div>";
                    linkMuller = "<div><button class='button button4' onclick='event.stopPropagation(); window.location.href = \"/books/muller/" + bookId 
                                                                   + "?word=" 
                                                                   + translateArray[i].name 
                                                                   + "\"'><i class='fa-solid fa-glasses'></i></button></div>";
                    let hintText = "";
                    if (Array.isArray(translateArray[i].bodyTranslate)) {
                        for (let j in translateArray[i].bodyTranslate) {
                            let entry = translateArray[i].bodyTranslate[j].ts_headline; 
                            if (fuzzyMatch(entry.replace(/<\/?b>/g, "").replace(/\n/g, "").split(" ").slice(0, 1).join(" "), translateArray[i].name)) {
                                translateArray[i].bodyTranslate[j].ts_headline = translateArray[i].bodyTranslate[0].ts_headline;
                                translateArray[i].bodyTranslate[0].ts_headline = entry;
                                break;
                            }
                        }
                        hintText = "- " + translateArray[i].bodyTranslate[0].ts_headline.replace(/<\/?b>/g, "").split(" ").slice(0, 8).join(" ");
                    }
                    let detailsText = "<span id='" + spanTranslateId + "' class='hide'><div class='container-word'>";
                    for (let j in translateArray[i].bodyTranslate) {
                        detailsText += translateArray[i].bodyTranslate[j].ts_headline;
                        detailsText += "<br><br>";
                    }
                    detailsText += "</div></span>";
                    translateText += "<div class='translate-span-block'>";
                    translateText += "<div id='" + spanOpenId + "' class='container-centered'>" + 
                        linkCurrentBook + 
                        linkMuller + 
                        " <div class='container-word'><b>" + translateArray[i].name + "</b> <i>" + hintText + "</i></div></div>";
                    translateText += detailsText;
                    translateText += "</div>";

                }

                getTranslation(translatePoint).innerHTML = translateText;
                getTranslation(translatePoint).className = "block translate-block";
                for (let i = 0; i < translateArray.length; i++) {
                    document.getElementById("s" + translatePoint + "-o" + i).onclick = function () {
                        if (document.getElementById("s" + translatePoint + "-t" + i).className === "hide") {
                            document.getElementById("s" + translatePoint +  "-t" + i).className = "";
                        } else {
                            document.getElementById("s" + translatePoint + "-t" + i).className = "hide";
                        }
                    };
                }

                document.getElementById("prompt" + translatePoint).addEventListener('keydown', function(event) {
                    event.stopPropagation();
                    if (event.code === "Enter") {
                        event.preventDefault();
                        document.getElementById('button_llm_any' + translatePoint).dispatchEvent(new Event('click'));
                    }
                });
                document.getElementById("prompt" + translatePoint).addEventListener('keyup', function(event) {
                    autoExpand(this);
                });

                document.getElementById("prompt" + translatePoint).value = text.trim() + ". In mentioned sentence explain very briefly the meaning ";
                document.getElementById("prompt" + translatePoint).setAttribute('oldHeight', 1 * document.getElementById("prompt" + translatePoint).style.height.replace('px', ''));
                document.getElementById("prompt" + translatePoint).dispatchEvent(new Event('keyup'));
                document.getElementById('button_llm_speaker' + translatePoint).onclick = async function () {
                    event.stopPropagation();
                    let speaker = document.getElementById('speaker_llm' + translatePoint);
                    if (speaker.paused) {
                        speaker.play();
                    } else {
                        speaker.pause();
                    }
                }
                document.getElementById('button_llm_explain' + translatePoint).onclick = async function () {
                    event.stopPropagation();
                    document.getElementById("prompt" + translatePoint).value = getSpan(currentPoint).textContent.trim() + ". In mentioned sentence explain very briefly the meaning ";
                }
                document.getElementById('button_llm_rephrase' + translatePoint).onclick = async function () {
                    event.stopPropagation();
                    document.getElementById("prompt" + translatePoint).value = getSpan(currentPoint).textContent.trim() + ". Rephrase very briefly mentioned sentence ";
                }
                document.getElementById('button_llm_idioms' + translatePoint).onclick = async function () {
                    event.stopPropagation();
                    document.getElementById("prompt" + translatePoint).value = getSpan(currentPoint).textContent.trim() + ". In mentioned sentence explain very briefly idioms or metaphors if exist in mentioned sentence ";
                }
                document.getElementById('button_llm_verbs' + translatePoint).onclick = async function () {
                    event.stopPropagation();
                    document.getElementById("prompt" + translatePoint).value = getSpan(currentPoint).textContent.trim() + ". In mentioned sentence explain very briefly phrasal verbs if exist ";
                }
                document.getElementById('button_llm_any' + translatePoint).onclick = async function () {
                    event.stopPropagation();
                    this.disabled = true;
                    await fetchAndStreamData(translatePoint,  document.getElementById("prompt" + translatePoint).value);
                }
                document.getElementById(closeSpanId).onclick = function () {
                    getTranslation(translatePoint).innerHTML = "";
                    getTranslation(translatePoint).className = "hide";
                    if (!isElementFullyVisible(getSpan(currentPoint))) {
                        getSpan(currentPoint).scrollIntoView({behavior: "auto", block: "start"});
                    }
                    selectSpan(currentPoint);
                };

                selectSpan(currentPoint);
            }
        };
    } else {
        document.getElementById(closeSpanId).onclick();
    }
}

function setHotKeys(event) {
    if (event.code === "KeyS") {
        getSpan(currentPoint).onclick();
        event.stopPropagation();
        playOrPauseAction();
    }
    if (event.code === "Space") {
        event.preventDefault();
        event.stopPropagation();
        playOrPauseAction();
    }
    else if (event.code === "KeyT") {
        if (!isElementFullyVisible(getSpan(currentPoint))) {
            getSpan(currentPoint).scrollIntoView({behavior: "auto", block: "start"});
        }
        selectSpan(currentPoint);
    }
    else if (event.code === "Backspace") {
        event.preventDefault();
        event.stopPropagation();
        translateAction();
    }
    else if (event.code === "ArrowLeft" || event.code === "KeyA") {
        event.preventDefault();
        event.stopPropagation();
        backAction();
    }
    else if (event.code === "ArrowRight" || event.code === "KeyD") {
        event.preventDefault();
        event.stopPropagation();
        nextAction();
    }
    else if (event.code === "KeyO") {
        event.preventDefault();
        event.stopPropagation();
        window.location.href = "/books/statistics/" + bookId;
    }
    else if (event.code === "KeyP") {
        event.preventDefault();
        event.stopPropagation();
        window.location.href = "/books/settings/" + bookId;
    }
    else if (event.code === "KeyM") {
        event.preventDefault();
        event.stopPropagation();
        window.location.href = "/books/muller/" + bookId + "?word=";
    }
    else if (event.code === "KeyH"){
        event.preventDefault();
        event.stopPropagation();
        window.location.href = "/books/history/" + bookId;
    }
    else if (event.code === "Slash") {
        event.preventDefault();
        event.stopPropagation();
        window.location.href = "/books/search_span/" + bookId;
    }
}

async function backAction () {
    if (checkStart(currentPoint)) {
        changePosition("back", 1);
    }
}

async function nextAction() {
    if (checkEnd(currentPoint)){
        changePosition("next", 1);
    }
}

function isElementFullyVisible(el) {
    let el_top = el.getBoundingClientRect().top;
    let el_bottom = el.getBoundingClientRect().bottom;
    let parent_top = readDiv.getBoundingClientRect().top;
    let parent_bottom = readDiv.getBoundingClientRect().bottom;

    if (el_top < parent_top) {
        return false;
    }
    if (el_bottom > parent_bottom) {
        return false;
    }
    return true;
}

async function play () {
    let endPromise;
    while (true) {
        if (!getSpan(currentPoint)) {
            await getAdditionalDownSpansPromises(50);
        }
        if (!getSpan(currentPoint)) {
            playButton.innerHTML = "<i class='fa-solid fa-play'></i>";
            break;
        }
        if (!isElementFullyVisible(getSpan(currentPoint))) {
            getSpan(currentPoint).scrollIntoView({behavior: "auto", block: "start"});
        }
        updateVars(bookId, currentPoint)
        createAudioIfNotExist(currentPoint);
        endPromise = new Promise(function (resolve) {
            var playPromise = playlist.get(currentPoint).play();
                if (playPromise !== undefined) {
                    playPromise.catch(error => {
                      // avoid this error
                      // Uncaught (in promise) DOMException: The play() request was interrupted because the media was removed from the document. 
                });
            }
            playlist.get(currentPoint).onended = resolve;
            playlist.get(currentPoint).onerror = resolve;
        });
        if (checkEnd(currentPoint) && !playlist.has(currentPoint + 1)) {
            createAudioIfNotExist(currentPoint + 1);
        }
        selectSpan(currentPoint);
        await endPromise;
        currentPoint++;
    }
}

function getSpan(pos) {
    return document.getElementById("s" + pos);
}

function getTranslation(pos) {
    return document.getElementById("t" + pos);
}

function getTtsUrl(span) {
    return "/text/get_audio?text=" + encodeURIComponent(span.textContent) + "&voice=" + 
    currentVoice + "&speed=" + currentSpeed;
}

function checkStart(pos) {
    return !(pos === parseInt(spans[0].id.slice(1)));
}

function checkEnd(pos) {
    return !(pos === parseInt(spans[spans.length - 1].id.slice(1)));
}

function onStartOfAudio(point) {
    playlist.get(point).currentTime = 0;
}

function createAudioIfNotExist(pos) {
    if (!playlist.has(pos))
        playlist.set(pos, new Audio(getTtsUrl(getSpan(pos))));
}

async function changePosition(action, num) {
    let wasPlaying = false;
    if (playlist.has(currentPoint)) {
        if (!playlist.get(currentPoint).paused) {
            playlist.get(currentPoint).pause();
            wasPlaying = true;
        }
        onStartOfAudio(currentPoint);
    }

    switch (action) {
        case "next":
			currentPoint += num;
			if (!isElementFullyVisible(getSpan(currentPoint))) {
				getSpan(currentPoint).scrollIntoView({behavior: "auto", block: "start"});
			}
			if (wasPlaying) {
				getSpan(currentPoint).onclick();
				event.stopPropagation();
				playOrPauseAction();
			}
            break;
        case "back":
			currentPoint -= num;
			if (!isElementFullyVisible(getSpan(currentPoint))) {
				getSpan(currentPoint).scrollIntoView({behavior: "auto", block: "start"});
			}
			if (wasPlaying) {
				getSpan(currentPoint).onclick();
				event.stopPropagation();
				playOrPauseAction();
			}
            break;
        case "specific":
            currentPoint = parseInt(num);
    }

    playButton.innerHTML = "<i class='fa-solid fa-play'>";
    selectSpan(currentPoint);
    updateVars(bookId, currentPoint)
}

async function selectSpan(pos) {
    let span = getSpan(pos);
    let beg = span.firstChild.textContent.search(/\S/);
    let end = span.firstChild.textContent.length - span.firstChild.textContent.split("").reverse().join("").search(/\S/);
    range.setStart(span.firstChild, beg);
    range.setEnd(span.firstChild, end);
    selection.removeAllRanges();
    selection.addRange(range);
    progBar.style.width = (pos / spanCount * 100) + "%";
    navigator.mediaSession.setActionHandler('play', playOrPauseAction);
    navigator.mediaSession.setActionHandler('pause', playOrPauseAction);
    navigator.mediaSession.setActionHandler('nexttrack', nextAction);
    navigator.mediaSession.setActionHandler('previoustrack', backAction);
}

async function updateVars(bookId, pos) {
    const xhttpUpdateVars = new XMLHttpRequest();
    xhttpUpdateVars.open("GET", "/text/save_vars?book_id=" + bookId +"&current_point=" + pos);
    xhttpUpdateVars.send();
}

function resetSpans() {
    spans = document.querySelectorAll("span.read-span");
    for (let i = 0; i < spans.length; i++) {
        spans[i].onclick = async function () {
            changePosition("specific", spans[i].id.slice(1));
        };
    }
}

function getAdditionalDownSpansPromises(newPointsCount, changeTime = 500) { 
return new Promise(function(resolveDown) {
        let newSpans = [];
        const xhttpNewSpans = new XMLHttpRequest();
        const lastCurrentPoint = parseInt(spans[spans.length - 1].id.slice(1));
        xhttpNewSpans.open("GET", "/text/get_spans?point=" + (lastCurrentPoint + 1) + "&count=" + newPointsCount + 
                "&navigation=" + 1 + "&table_id=" + bookId);
        xhttpNewSpans.send();

        xhttpNewSpans.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
                    newSpans = JSON.parse(this.response);
                if (newSpans.length === 0) {
                    bookEnd = true;
                    resolveDown();
                }
                else if (!bookEnd && !getSpan(newSpans[0]['id'])) {
                    let htmlNewSpans = "";
                    for (let i = 0; i < newSpans.length; i++) {
                        htmlNewSpans += "<span id='s" + newSpans[i]['id'] +"' class='read-span'>" + newSpans[i]['span'] + "</span>";
                        htmlNewSpans += "<div id='t" + newSpans[i]['id'] + "' class='block hide translate-block'></div>";
                    }
                    spanCollection.innerHTML = spanCollection.innerHTML + htmlNewSpans;
                    resetSpans();
                    setTimeout(resolveDown(), changeTime);
                }
            }
        };
    });
}

function getAdditionalUpSpansPromises(newPointsCount, changeTime = 500) { 
    return new Promise(function(resolveUp) {
        let newSpans = [];
        const xhttpNewSpans = new XMLHttpRequest();
        const startCurrentPoint = parseInt(spans[0].id.slice(1));

        xhttpNewSpans.open("GET", "/text/get_spans?point=" + (startCurrentPoint - 1) + "&count=" + newPointsCount + 
                "&navigation=" + 0 + "&table_id=" + bookId);
        xhttpNewSpans.send();

        xhttpNewSpans.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                newSpans = JSON.parse(this.response);
                if (newSpans.length === 0) {
                    bookStart = true;
                    resolveUp();
                }
                else if (!bookStart && newSpans.length !== 0 && !getSpan(newSpans[0]['id'])) {
                    let htmlNewSpans = "";
                    for (let i = 0; i < newSpans.length; i++) {    
                        htmlNewSpans += "<span id='s" + newSpans[i]['id'] +"' class='read-span'>" + newSpans[i]['span'] + "</span>";
                        htmlNewSpans += "<div id='t" + newSpans[i]['id'] + "' class='block hide translate-block'></div>";
                    }

                    spanCollection.innerHTML = htmlNewSpans + spanCollection.innerHTML;
                    resetSpans();
                    setTimeout(resolveUp(), changeTime);
                    getSpan(startCurrentPoint).scrollIntoView({behavior: "auto", block: "start"});
                }
            }
        };
    });
}

async function fetchAndStreamData(id, text) {
    const dataContainer = document.getElementById("prompt" + id);

    dataContainer.value = '';
    dataContainer.dispatchEvent(new Event('keyup'));

    const response = await fetch('/ollama.php', {
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify({prompt: text})
    });

    if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
    }

    const reader = response.body.getReader();
    const decoder = new TextDecoder();
    
    while (true) {
        const { done, value } = await reader.read();
        if (done) break;

        // Decode the chunk and append it to the div
        const chunk = decoder.decode(value, { stream: true });
        dataContainer.value += chunk;
        dataContainer.dispatchEvent(new Event('keyup'));
    }

    document.getElementById('button_llm_any' + id).disabled = false;
    document.getElementById('speaker_llm' + id).src = "/text/get_audio?voice=slt+anna&speed=" + currentSpeed + "&text=" + encodeURIComponent(dataContainer.value);
    beep(350, 300, 1);
}

function reduceAbit(obj) {
    oldScroll = obj.scrollHeight;
    oldHeight = obj.style.height;
    obj.style.height = Math.max(1 * obj.style.height.replace('px', '') - 10, obj.getAttribute('oldHeight')) + 'px';
    if (oldScroll === obj.scrollHeight) {
        obj.style.height = oldHeight;
        return false;
    } else {
        return true;
    }
}

function autoExpand(obj) {
    if (obj.value.length < obj.getAttribute('oldLength')) {
        while (reduceAbit(obj)) {
        }
    }
    if (obj.scrollHeight > 1 * obj.style.height.replace('px', '')) obj.style.height = obj.scrollHeight + 8 + 'px';
    obj.setAttribute('oldLength', obj.value.length);
}

function beep(duration, frequency, volume) {
    var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
    var oscillator = audioCtx.createOscillator();
    var gainNode = audioCtx.createGain();

    oscillator.connect(gainNode);
    gainNode.connect(audioCtx.destination);

    gainNode.gain.value = volume;
    oscillator.frequency.value = frequency;
    oscillator.type = "sine";

    oscillator.start();
    setTimeout(function() {
        oscillator.stop();
    }, duration);
}

