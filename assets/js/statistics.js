const bookId = parseInt(document.getElementById("book-id").getAttribute("info"));
window.onkeydown = setHotKeys;

function setHotKeys(event) {
    if (event.code === "KeyO") {
        event.preventDefault();
        event.stopPropagation();
        window.location.href = "/books/show/" + bookId;
    } else if (event.code === "KeyP") {
        event.preventDefault();
        event.stopPropagation();
        window.location.href = "/books/settings/" + bookId;
    } else if (event.code === "KeyH") {
        event.preventDefault();
        event.stopPropagation();
        window.location.href = "/books/history/" + bookId;
    } else if (event.code === "Slash") {
        event.preventDefault();
        event.stopPropagation();
        window.location.href = "/books/show/" + bookId;
    }

}
