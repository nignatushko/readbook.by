const nonActiveBackDiv = document.getElementById("non-active-back");
const ldsRoller = document.getElementById("lds-roller");

window.onbeforeunload = function ()
{
    nonActiveBackDiv.classList.remove("hide");
    ldsRoller.classList.remove("hide");
}