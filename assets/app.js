import './styles/app.css';

import './bootstrap';

const showPersonalOptions = document.getElementById('show-personal-options');
const personalOptions = document.getElementById('personal-options');

showPersonalOptions.onclick = function ()
{
    personalOptions.classList.toggle('hide');
    showPersonalOptions.classList.toggle('black-back');
}

personalOptions.onmouseover = function ()
{
    personalOptions.classList.remove('hide');
    showPersonalOptions.classList.add('black-back');
}

personalOptions.onmouseout = function ()
{
    personalOptions.classList.add('hide');
    showPersonalOptions.classList.remove('black-back');
}